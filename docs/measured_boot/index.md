!!! warning "Disclaimer"

    This installation guide, **called "guide" in the following**, builds upon [the official Gentoo Linux installation handbook](https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation).

    It's written with great care. Nevertheless, you are **expected not to blindly copy&paste commands!** Please, **understand** what you are going to do **and adjust commands if required!**

    Even with the whole codeblock being a no-brainer, **some codeblocks need to be copied and pasted "command block" by "command block"**. In this case, **a [copy button :material-content-copy:](https://squidfunk.github.io/mkdocs-material/reference/code-blocks/#code-copy-button) won't be offered** and you'll see a note saying "copy&paste one command block after the other". A **copy button also won't be shown for codeblocks containing sample output**. They start with "❯ ".

    In the following, I provide an **example of a codeblock from chapter [8.1. Basic Configuration](bootup_setup.md#81-basic-configuration) containing two "command blocks"**. Based upon that codeblock, you are expected:

    1. ... to copy&paste the second line of the codeblock, edit the value of `my_hostname` to your liking and set the variable by pressing ++enter++.
    2. ... to copy&paste the commands interconnected with `&& \` (meaning line four to six) and execute them by pressing ++enter++.

    !!! example ""

        <figure markdown="span">
            ![command block example](../images/command_block_example.png)
        </figure>

    Unless otherwise specified, **all commands are executed as root**.

!!! danger "Danger of soft-bricking your device"

    Please, read chapter [5.2 Enrolling Option ROM digests](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot#Enrolling_Option_ROM_digests) of the Arch Linux wiki, [sbctl's wiki entry on Option ROM](https://github.com/Foxboron/sbctl/wiki/FAQ#option-rom) as well as [Ajak's Wiki entry on Measured Boot](https://wiki.gentoo.org/wiki/User:Ajak/Measured_Boot#Option_Roms) and make sure to fully understand the role of `Microsoft 3rd Party UEFI CA certificate` and whether your device requires it. **If necessary, take appropriate measures to avoid soft-bricking your device.**

## 1.1. Developer Contacts

!!! info "GnuPG Public Key"

    You can find information on my GnuPG public key in my [Codeberg profile README](https://codeberg.org/duxsco/)!

You can contact me over [these channels](https://keyoxide.org/noreply.keyoxide@duxsco.de). In case of bugs or feature requests, [you can open an issue](https://codeberg.org/duxsco/gentoo-installation/issues), too.

## 1.2. Documentation State

!!! info ":material-rss: RSS feed"

    You can subscribe to the [:material-rss: RSS feed](https://codeberg.org/duxsco/gentoo-installation/releases.rss) on [releases](https://codeberg.org/duxsco/gentoo-installation/releases) to be notified of new (stable) releases of the guide.

If I have sufficient time, I plan to conduct a **test install around every 3-4 months** to ensure that the guide still works as expected. Some challenging aspects of this guide may not be covered due to time constraints, e.g. the build of non-binary kernels. I open issues for **bugs**, that I find and that are not yet resolved in a [release](https://codeberg.org/duxsco/gentoo-installation/releases), **on [this page](https://codeberg.org/duxsco/gentoo-installation/issues)**.

The **latest test install has been conducted on October 29th-30th 2024** with "SystemRescue 11.01 for amd64" and:

- `stage3-amd64-hardened-systemd-20250209T170331Z.tar.xz` which you may still find on this [mirror](https://distfiles.gentoo.org/releases/amd64/autobuilds/) or one of the mirrors mentioned [here](https://bugs.gentoo.org/834712)

- the following repo ([link to commit "c0ec6a8c9e2a7c0a2ccf8a250113f6e24a2cc51c"](https://gitweb.gentoo.org/repo/gentoo.git/commit/?id=c0ec6a8c9e2a7c0a2ccf8a250113f6e24a2cc51c):
``` { .shell .no-copy linenums="1" }
[root@sysrescue ~]# cat /mnt/gentoo/var/db/repos/gentoo/metadata/timestamp.commit
c0ec6a8c9e2a7c0a2ccf8a250113f6e24a2cc51c 1739415398 2025-02-13T02:56:38Z
```

You can find some info on the repo's Git mirrors [here](https://wiki.gentoo.org/wiki/Project:Portage/Repository_verification#Using_it).

## 1.3. Local Hosting

I recommend the download of the latest [release](https://codeberg.org/duxsco/gentoo-installation/releases) of the guide, verification of SHA256/SHA512 hashes as well as OpenPGP signatures and running the guide's website locally with `mkdocs serve`. This prevents MITM attacks. Installation of "Material for MkDocs" is described in the [repo's README.md](https://codeberg.org/duxsco/gentoo-installation/src/branch/main/README.md).

## 1.4. Technologies

The guide results in a system that is/uses:

- [x] **Secure Boot**: All EFI binaries and unified kernel images are signed.
- [x] **Measured Boot**: [systemd-cryptenroll](https://wiki.archlinux.org/title/Systemd-cryptenroll) or [clevis](https://github.com/latchset/clevis) is used to check the system for manipulations via TPM 2.0 PCRs.
- [x] **Fully encrypted**: Except for ESP(s), all partitions are LUKS encrypted.
- [x] **RAID**: Except for ESP(s), btrfs and mdadm based RAID are used for all partitions if the number of disks is ≥2.
- [x] **Rescue system**: A customised SystemRescue supports optional SSH logins and provides a convenient [chroot.sh](https://codeberg.org/duxsco/gentoo-installation/src/branch/main/bin/disk.sh#L285-L364) script.
- [x] **Hardened Gentoo Linux** for a highly secure, high stability production environment ([link](https://wiki.gentoo.org/wiki/Project:Hardened)).
- [x] **SELinux (optional)** provides Mandatory Access Control using type enforcement and role-based access control ([link](https://wiki.gentoo.org/wiki/Project:SELinux)).

## 1.5. System Requirements

Beside [official hardware requirements](https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation#Hardware_requirements), the guide has additional ones:

- Only **UEFI** boot has been tested and is supported by this guide. You should have a directory `/sys/firmware/efi/` with some content:
``` { .shell .no-copy linenums="1" }
❯ ls /sys/firmware/efi/
config_table  efivars  fw_platform_size  fw_vendor  runtime  runtime-map  systab
```
- The guide builds heavily on **Secure Boot**. Without that, Measured Boot cannot be used as intended. Make sure that the system is in "setup mode" in order to be able to add your custom Secure Boot keys.
``` { .shell .no-copy linenums="1" }
❯ bootctl status 2>/dev/null | grep -i "secure boot"
   Secure Boot: disabled (setup)

❯ pacman -Sy sbctl
❯ sbctl status
Installed:      ✗ sbctl is not installed
Setup Mode:     ✗ Enabled
Secure Boot:    ✗ Disabled
Vendor Keys:    none
```
- **TPM 2.0** is required for Measured Boot:
``` { .shell .no-copy linenums="1" }
❯ bootctl status 2>/dev/null | grep -i "tpm2 support"
  TPM2 Support: yes

❯ systemd-cryptenroll --tpm2-device=list
PATH        DEVICE      DRIVER
/dev/tpmrm0 MSFT0101:00 tpm_tis
```
- **systemd:** The guide requires the use of systemd for Measured Boot to work without restrictions. [Clevis](https://github.com/latchset/clevis) may be an option if you want to stay with OpenRC. But, I haven't tested this.
- **x86_64 Architecture:** To keep things simple, the guide presumes that you intend to install on a x86_64 system. This is the only architecture that has been tested by me! And, it's the only architecture still [actively supported by SystemRescue](https://www.system-rescue.org/Download/). SystemRescue is used for the rescue system with its custom [chroot.sh script](https://codeberg.org/duxsco/gentoo-installation/src/branch/main/bin/disk.sh#L285-L364). Beware, however, that the [switch to the official Gentoo Linux ISOs](https://codeberg.org/duxsco/gentoo-installation/issues/4) is in planning in order to support more architectures in the long run and have additional features added.

## 1.6. SSH Connectivity

After completion of this guide, optional SSH connections will be possible to the following systems using SSH public key authentication:

!!! example ""

    === "Gentoo Linux installation"
        ``` { .shell .no-copy linenums="1" }
        ssh -p 50022 your_non-root_user@<IP address>
        ```

    === "Rescue System"
        ``` { .shell .no-copy linenums="1" }
        ssh -p 50023 root@<IP address>
        ```

## 1.7. Disk Layout

Independent ESPs are created one for each disk to provide for redundancy, because there is the risk of data corruption with the redundancy provided by mdadm RAID (further info: [6.1 ESP on software RAID1](https://wiki.archlinux.org/title/EFI_system_partition#ESP_on_software_RAID1)). Except for ESPs, [btrfs](https://btrfs.readthedocs.io/en/latest/mkfs.btrfs.html#profiles) or [mdadm](https://raid.wiki.kernel.org/index.php/Introduction#The_RAID_levels) based RAID 1 is used for all other partitions on a dual- or multi-disk setup with RAID 5, RAID 6 and RAID 10 being further options for the swap device. The 2nd partition doesn't make use of btrfs RAID due to [limitations of SystemRescue](https://gitlab.com/systemrescue/systemrescue-sources/-/issues/292#note_1036225171).

!!! example ""

    === "four disks"

        ``` { .text .no-copy }
        PC/Laptop──────────────────────────┐
        ├── /dev/sda                       └── /dev/sdb
        │   ├── 1. EFI System Partition        ├── 1. EFI System Partition
        │   ├── 2. MDADM RAID 1                ├── 2. MDADM RAID 1
        │   │   └── LUKS                       │   └── LUKS
        │   │       └── Btrfs                  │       └── Btrfs
        │   │           └── rescue             │           └── rescue
        │   ├── 3. LUKS                        ├── 3. LUKS
        │   │   └── MDADM RAID 1|5|6|10        │   └── MDADM RAID 1|5|6|10
        │   │       └── SWAP                   │       └── SWAP
        │   └── 4. LUKS ("system" partition)   └── 4. LUKS ("system" partition)
        │       └── Btrfs raid1c4                  └── Btrfs raid1c4
        │           └── subvolume                      └── subvolume
        │               ├── @binpkgs                       ├── @binpkgs
        │               ├── @distfiles                     ├── @distfiles
        │               ├── @home                          ├── @home
        │               ├── @ebuilds                       ├── @ebuilds
        │               ├── @root                          ├── @root
        │               └── @var_tmp                       └── @var_tmp
        │
        ├──────────────────────────────────┐
        └── /dev/sdc                       └── /dev/sdd
            ├── 1. EFI System Partition        ├── 1. EFI System Partition
            ├── 2. MDADM RAID 1                ├── 2. MDADM RAID 1
            │   └── LUKS                       │   └── LUKS
            │       └── Btrfs                  │       └── Btrfs
            │           └── rescue             │           └── rescue
            ├── 3. LUKS                        ├── 3. LUKS
            │   └── MDADM RAID 1|5|6|10        │   └── MDADM RAID 1|5|6|10
            │       └── SWAP                   │       └── SWAP
            └── 4. LUKS ("system" partition)   └── 4. LUKS ("system" partition)
                └── Btrfs raid1c4                  └── Btrfs raid1c4
                    └── subvolume                      └── subvolume
                        ├── @binpkgs                       ├── @binpkgs
                        ├── @distfiles                     ├── @distfiles
                        ├── @home                          ├── @home
                        ├── @ebuilds                       ├── @ebuilds
                        ├── @root                          ├── @root
                        └── @var_tmp                       └── @var_tmp
        ```

    === "three disks"

        ``` { .text .no-copy }
        PC/Laptop──────────────────────────┐
        ├── /dev/sda                       └── /dev/sdb
        │   ├── 1. EFI System Partition        ├── 1. EFI System Partition
        │   ├── 2. MDADM RAID 1                ├── 2. MDADM RAID 1
        │   │   └── LUKS                       │   └── LUKS
        │   │       └── Btrfs                  │       └── Btrfs
        │   │           └── rescue             │           └── rescue
        │   ├── 3. LUKS                        ├── 3. LUKS
        │   │   └── MDADM RAID 1|5             │   └── MDADM RAID 1|5
        │   │       └── SWAP                   │       └── SWAP
        │   └── 4. LUKS ("system" partition)   └── 4. LUKS ("system" partition)
        │       └── Btrfs raid1c3                  └── Btrfs raid1c3
        │           └── subvolume                      └── subvolume
        │               ├── @binpkgs                       ├── @binpkgs
        │               ├── @distfiles                     ├── @distfiles
        │               ├── @home                          ├── @home
        │               ├── @ebuilds                       ├── @ebuilds
        │               ├── @root                          ├── @root
        │               └── @var_tmp                       └── @var_tmp
        │
        │
        └── /dev/sdc
            ├── 1. EFI System Partition
            ├── 2. MDADM RAID 1
            │   └── LUKS
            │       └── Btrfs
            │           └── rescue
            ├── 3. LUKS
            │   └── MDADM RAID 1|5
            │       └── SWAP
            └── 4. LUKS ("system" partition)
                └── Btrfs raid1c3
                    └── subvolume
                        ├── @binpkgs
                        ├── @distfiles
                        ├── @home
                        ├── @ebuilds
                        ├── @root
                        └── @var_tmp
        ```

    === "two disks"

        ``` { .text .no-copy }
        PC/Laptop──────────────────────────┐
        └── /dev/sda                       └── /dev/sdb
            ├── 1. EFI System Partition        ├── 1. EFI System Partition
            ├── 2. MDADM RAID 1                ├── 2. MDADM RAID 1
            │   └── LUKS                       │   └── LUKS
            │       └── Btrfs                  │       └── Btrfs
            │           └── rescue             │           └── rescue
            ├── 3. LUKS                        ├── 3. LUKS
            │   └── MDADM RAID 1               │   └── MDADM RAID 1
            │       └── SWAP                   │       └── SWAP
            └── 4. LUKS ("system" partition)   └── 4. LUKS ("system" partition)
                └── Btrfs raid1                    └── Btrfs raid1
                    └── subvolume                      └── subvolume
                        ├── @binpkgs                       ├── @binpkgs
                        ├── @distfiles                     ├── @distfiles
                        ├── @home                          ├── @home
                        ├── @ebuilds                       ├── @ebuilds
                        ├── @root                          ├── @root
                        └── @var_tmp                       └── @var_tmp
        ```

    === "single disk"

        ``` { .text .no-copy }
        PC/Laptop
        └── /dev/sda
            ├── 1. EFI System Partition
            ├── 2. LUKS
            │   └── Btrfs (single)
            │       └── rescue
            ├── 3. LUKS
            │   └── SWAP
            └── 4. LUKS ("system" partition)
                └── Btrfs (single)
                    └── subvolumes
                        ├── @binpkgs
                        ├── @distfiles
                        ├── @home
                        ├── @ebuilds
                        ├── @root
                        └── @var_tmp
        ```

## 1.8. LUKS Key Slots

On the "rescue" partition, LUKS key slots are set as follows:

  - 0: Rescue password

On all other LUKS volumes, LUKS key slots are set as follows:

  - 0: Fallback password for emergency
  - 1: Measured Boot
    - Option A: TPM 2.0 with optional pin to unlock with [systemd-cryptenroll](https://wiki.archlinux.org/title/Systemd-cryptenroll)
    - Option B: [Shamir Secret Sharing](https://github.com/latchset/clevis#pin-shamir-secret-sharing) combining [TPM 2.0](https://github.com/latchset/clevis#pin-tpm2) and [Tang](https://github.com/latchset/clevis#pin-tang) pin ([Tang project](https://github.com/latchset/tang)) to automatically unlock with Clevis
