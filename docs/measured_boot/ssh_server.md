!!! warning "Export "non_root" variable"

    In this chapter, make sure to always have the `non_root` environment variable set:

    ```shell linenums="1"
    export non_root=$(id -nu 1000)
    ```

    This variable will be used to setup your non-root user.

!!! note
    If you haven't setup [SSH for the SystemRescue system](rescue_system.md#43-optional-ssh-server), you have to create "~/.ssh/authorized_keys" inside of your non-root's home folder manually instead of copying out of "/etc/gentoo-installation/systemrescuecd/" as suggested in the following codeblock.

Take care of [public key authentication](https://wiki.gentoo.org/wiki/SSH#Passwordless_authentication_to_a_distant_SSH_server):

```shell linenums="1"
rsync -av --chown="${non_root}": /etc/gentoo-installation/systemrescuecd/recipe/build_into_srm/root/.ssh/authorized_keys "/home/${non_root}/.ssh/" && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Configure the SSH server:

```shell linenums="1"
echo "\
Port 50022
X11Forwarding no
AuthenticationMethods publickey

AllowUsers ${non_root}" > /etc/ssh/sshd_config.d/9999999my-custom.conf && \
ssh-keygen -A && \
sshd -t && \
systemctl --no-reload enable sshd.service && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

!!! note
    For "dispatch-conf" to work for the following change in "/usr/local/sbin", you need to execute the following command after creation and modification of "/usr/local/sbin/._cfg0000_firewall.nft":
    
    ```shell linenums="1"
    dispatch-conf /usr/local/sbin
    ```

Open the SSH port:

```shell hl_lines="1" linenums="1"
rsync -a /usr/local/sbin/firewall.nft /usr/local/sbin/._cfg0000_firewall.nft && \
sed -i 's/^#\([[:space:]]*\)tcp dport 50022 ct state new accept$/\1tcp dport 50022 ct state new accept/' /usr/local/sbin/._cfg0000_firewall.nft && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Write down fingerprints to double check upon initial SSH connection to the Gentoo Linux machine:

```shell linenums="1"
find /etc/ssh/ -type f -name "ssh_host*\.pub" -exec ssh-keygen -vlf {} \;
```
