!!! info "Application of configuration changes"
    Starting with this chapter, **execute [dispatch-conf](https://wiki.gentoo.org/wiki/Dispatch-conf) after every codeblock** where a [".\_cfg0000_" prefixed file](https://projects.gentoo.org/pms/8/pms.html#x1-14600013.3.3) has been created. <span style="background-color: #4287ff1a;">The creation of ".\_cfg0000_" prefixed files will be highlighted in blue</span> (see first line in the next codeblock below).

Make "dispatch-conf" show [diffs in color](https://wiki.gentoo.org/wiki/Dispatch-conf#Changing_diff_or_merge_tools) and use [vimdiff for merging](https://wiki.gentoo.org/wiki/Dispatch-conf#Use_.28g.29vimdiff_to_merge_changes):

```shell hl_lines="1" linenums="1"
rsync -a /etc/dispatch-conf.conf /etc/._cfg0000_dispatch-conf.conf && \
sed -i \
-e "s/diff=\"diff -Nu '%s' '%s'\"/diff=\"diff --color=always -Nu '%s' '%s'\"/" \
-e "s/merge=\"sdiff --suppress-common-lines --output='%s' '%s' '%s'\"/merge=\"vimdiff -c'saveas %s' -c next -c'setlocal noma readonly' -c prev %s %s\"/" \
/etc/._cfg0000_dispatch-conf.conf && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

## 6.1. Portage Configuration

Configure [make.conf](https://wiki.gentoo.org/wiki//etc/portage/make.conf) (copy&paste one command block after the other):

``` { .shell hl_lines="1" .no-copy linenums="1" }
rsync -av /etc/portage/make.conf /etc/portage/._cfg0000_make.conf

# If you use distcc, beware of:
# https://wiki.gentoo.org/wiki/Distcc#-march.3Dnative
#
# You could resolve "-march=native" with app-misc/resolve-march-native
sed -i 's/COMMON_FLAGS="-O2 -pipe"/COMMON_FLAGS="-march=native -O2 -pipe"/' /etc/portage/._cfg0000_make.conf

# https://wiki.gentoo.org/wiki/EMERGE_DEFAULT_OPTS
#
# For all other flags, take a look at "man emerge" or
# https://gitweb.gentoo.org/proj/portage.git/tree/man/emerge.1
echo 'EMERGE_DEFAULT_OPTS="--noconfmem --with-bdeps=y --complete-graph=y"' >> /etc/portage/._cfg0000_make.conf

# https://wiki.gentoo.org/wiki/Localization/Guide#L10N
# https://wiki.gentoo.org/wiki/Localization/Guide#LINGUAS
echo '
L10N="de"
LINGUAS="${L10N}"' >> /etc/portage/._cfg0000_make.conf

# distfiles.gentoo.org - the default - is supported by CDN77.
# I just change the default from http:// to https://.
# https://wiki.gentoo.org/wiki/GENTOO_MIRRORS
echo '
GENTOO_MIRRORS="https://distfiles.gentoo.org"' >> /etc/portage/._cfg0000_make.conf

# https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Portage#Fetch_commands
#
# Default values from /usr/share/portage/config/make.globals are:
# FETCHCOMMAND="wget -t 3 -T 60 --passive-ftp -O \"\${DISTDIR}/\${FILE}\" \"\${URI}\""
# RESUMECOMMAND="wget -c -t 3 -T 60 --passive-ftp -O \"\${DISTDIR}/\${FILE}\" \"\${URI}\""
#
# File in git: https://gitweb.gentoo.org/proj/portage.git/tree/cnf/make.globals
#
# They are insufficient in my opinion.
# Thus, I am enforcing TLSv1.2 or greater, secure TLSv1.2 cipher suites and https-only.
# TLSv1.3 cipher suites are secure. Thus, I don't set "--tls13-ciphers".
# Beware: Some (few?) packages cannot be fetched with "--proto =https".
#         media-sound/spotify, for example, is such a candidate.
echo 'FETCHCOMMAND="curl --fail --silent --show-error --location --proto =https --tlsv1.3 --retry 2 --connect-timeout 60 -o \"\${DISTDIR}/\${FILE}\" \"\${URI}\""
RESUMECOMMAND="${FETCHCOMMAND} --continue-at -"' >> /etc/portage/._cfg0000_make.conf

# Some useflags I set for personal use.
# Feel free to adjust as with any other codeblock. 😄
echo '
USE_HARDENED="caps -jit pie -sslv3 -suid"
USE="${USE_HARDENED}"' >> /etc/portage/._cfg0000_make.conf

# https://wiki.gentoo.org/wiki/Binary_package_guide#Implementing_buildpkg_as_a_Portage_feature
echo 'FEATURES="buildpkg"' >> /etc/portage/._cfg0000_make.conf
```

I prefer English manpages and ignore above [L10N](https://wiki.gentoo.org/wiki/Localization/Guide#L10N) setting for "sys-apps/man-pages". Makes using Stackoverflow easier :wink:.

```shell linenums="1"
echo "sys-apps/man-pages -l10n_de" >> /etc/portage/package.use/main && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

## 6.2. Repo Syncing

Do an initial sync:

```shell linenums="1"
emerge-webrsync
```

(Optional) I personally prefer syncing the repo via ["eix-sync"](https://wiki.gentoo.org/wiki/Eix#Method_2:_Using_eix-sync) which is provided by [app-portage/eix](https://wiki.gentoo.org/wiki/Eix). But, there is also [emaint](https://wiki.gentoo.org/wiki/Gentoo_Cheat_Sheet#Sync_methods):

!!! example ""

    === "eix-sync"
        ```shell linenums="1"
        emerge app-portage/eix && \
        eix-sync
        ```

    === "emaint (replaced "emerge --sync")"
        ```shell linenums="1"
        emaint --auto sync
        ```

Read [Gentoo news items](https://www.gentoo.org/glep/glep-0042.html):

```shell linenums="1"
eselect news list
# eselect news read 1
# eselect news read 2
# etc.
```

## 6.3. CPU flags

Set [CPU flags](https://wiki.gentoo.org/wiki/CPU_FLAGS_*#Using_cpuid2cpuflags):

```shell linenums="1"
emerge --oneshot app-portage/cpuid2cpuflags && \
echo "*/* $(cpuid2cpuflags)" > /etc/portage/package.use/00cpu-flags && \
echo -e "\e[1;32mSUCCESS\e[0m"
```
