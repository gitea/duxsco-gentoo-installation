Before rebooting, fetch "net-firewall/nftables" to be able to setup the firewall before connecting to the network with Gentoo Linux for the first time:

```shell linenums="1"
emerge --fetchonly net-firewall/nftables
```

## 11.1. Network

Configure the [network connection](https://wiki.gentoo.org/wiki/Systemd/systemd-networkd) (copy&paste one command block after the other):

=== "DHCP"

    ``` { .shell .no-copy linenums="1" }
    echo "\
    # Set the MAC address of your network interface
    [Match]
    MACAddress=aa:bb:cc:dd:ee:ff

    [Network]
    DHCP=yes
    " >> /etc/systemd/network/50-dhcp.network
    ```

=== "Static IP"

    ``` { .shell .no-copy linenums="1" }
    echo "\
    # Set the MAC address of your network interface
    [Match]
    MACAddress=aa:bb:cc:dd:ee:ff

    # Sample IP addresses from:
    # https://datatracker.ietf.org/doc/html/rfc5737
    # https://datatracker.ietf.org/doc/html/rfc3849
    #
    # Sample DNS server from Freifunk Munich:
    # https://ffmuc.net/wiki/doku.php?id=knb:dns
    #
    # Make sure to set correct addresses
    [Network]
    Address=192.168.2.2/24
    Address=2001:DB8::2/64
    Gateway=192.168.2.1
    Gateway=2001:DB8::1
    DNS=5.1.66.255
    DNS=185.150.99.255
    DNS=2001:678:e68:f000::
    DNS=2001:678:ed0:f000::
    " >> /etc/systemd/network/50-static.network
    ```

Enable the service:

```shell linenums="1"
systemctl --no-reload enable systemd-networkd.service
```

## 11.2. Reboot

Exit, cleanup obsolete installation files as well as [symlinks to devices created by "disk.sh"](https://codeberg.org/duxsco/gentoo-installation/src/branch/main/bin/disk.sh#L263-L282) and [reboot](https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation#Rebooting_the_system) (copy&paste one command block after the other):

``` { .shell .no-copy linenums="1" }
[[ -L /devRescue ]] && exit
[[ -L /devRescue ]] && exit
[[ -L /devRescue ]] && exit
cd
rm -fv /mnt/gentoo/{stage3.tar.xz{,.asc},devEfi*,devRescue,devSystem*,devSwap*,mapperRescue,mapperSwap,mapperSystem}
umount -l /mnt/gentoo/dev{/shm,/pts,}
umount -R /mnt/gentoo
reboot
```
