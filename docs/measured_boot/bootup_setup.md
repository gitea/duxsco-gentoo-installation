## 8.1. Basic Configuration

Setup [/etc/fstab](https://wiki.gentoo.org/wiki//etc/fstab):

```shell linenums="1"
SWAP_UUID=$(blkid -s UUID -o value /mapperSwap) && \
SYSTEM_UUID=$(blkid -s UUID -o value /mapperSystem) && \
echo "" >> /etc/fstab && \
echo "
$(while read -r i; do
  echo "UUID=$(blkid -s UUID -o value "$i") ${i/devE/boot\/e} vfat noatime,dmask=0027,fmask=0137 0 0"
done < <(find /devEfi* -maxdepth 0))
UUID=${SWAP_UUID}   none                 swap  sw                        0 0
UUID=${SYSTEM_UUID} /                    btrfs noatime,subvol=@root      0 0
UUID=${SYSTEM_UUID} /home                btrfs noatime,subvol=@home      0 0
UUID=${SYSTEM_UUID} /var/cache/binpkgs   btrfs noatime,subvol=@binpkgs   0 0
UUID=${SYSTEM_UUID} /var/cache/distfiles btrfs noatime,subvol=@distfiles 0 0
UUID=${SYSTEM_UUID} /var/db/repos/gentoo btrfs noatime,subvol=@ebuilds   0 0
UUID=${SYSTEM_UUID} /var/tmp             btrfs noatime,subvol=@var_tmp   0 0
" | column -o " " -t >> /etc/fstab && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Setup [/etc/hosts](https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/System#The_hosts_file) (copy&paste one command block after the other):

``` { .shell hl_lines="4" .no-copy linenums="1" }
# Set the hostname of your choice
my_hostname="micro"

rsync -a /etc/hosts /etc/._cfg0000_hosts && \
sed -i "s/localhost$/localhost ${my_hostname}/" /etc/._cfg0000_hosts && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Disable "magic SysRq" for [security sake](https://wiki.gentoo.org/wiki/Vlock#Disable_SysRq_key):

```shell linenums="1"
mkdir /etc/sysctl.d && \
echo "kernel.sysrq = 0" > /etc/sysctl.d/99sysrq.conf
```

(Optional) Install miscellaneous tools:

```shell linenums="1"
emerge -av app-misc/screen app-portage/gentoolkit
```

## 8.2. systemd Preparation

Apply systemd useflags:

```shell linenums="1"
touch /etc/sysctl.conf && \

# add LUKS volume and systemd-boot support
echo "sys-apps/systemd boot cryptsetup kernel-install" >> /etc/portage/package.use/main && \

emerge -avuDN @world
```

Do some [initial configuration](https://wiki.gentoo.org/wiki/Systemd#Configuration):

```shell linenums="1"
systemd-machine-id-setup && \
systemd-firstboot --prompt && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

If you **don't** plan to keep your setup slim for the later [SELinux setup](../optional_gentoo_guides/selinux.md), the use of preset files may be s.th. to consider:

> Most services are disabled when systemd is first installed. A "preset" file is provided, and may be used to enable a reasonable set of default services. ([source](https://wiki.gentoo.org/wiki/Systemd#Preset_services))

``` { .shell .no-copy linenums="1" }
systemctl preset-all
# or
systemctl preset-all --preset-mode=enable-only
```

## 8.3. Secure Boot

!!! danger "Danger of soft-bricking your device"

    Please, read chapter [5.2 Enrolling Option ROM digests](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot#Enrolling_Option_ROM_digests) of the Arch Linux wiki, [sbctl's wiki entry on Option ROM](https://github.com/Foxboron/sbctl/wiki/FAQ#option-rom) as well as [Ajak's Wiki entry on Measured Boot](https://wiki.gentoo.org/wiki/User:Ajak/Measured_Boot#Option_Roms) and make sure to fully understand the role of `Microsoft 3rd Party UEFI CA certificate` and whether your device requires it. **If necessary, take appropriate measures to avoid soft-bricking your device.**

Install "app-crypt/sbctl":

```shell linenums="1"
emerge -av app-crypt/sbctl
```

Create and enroll Secure Boot files ([link](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot#Creating_and_enrolling_keys)) (copy&paste one command block after the other):

``` { .shell .no-copy linenums="1" }
❯ sbctl status
Installed:      ✗ sbctl is not installed
Setup Mode:     ✗ Enabled
Secure Boot:    ✗ Disabled
Vendor Keys:    none

❯ sbctl create-keys
Created Owner UUID 4cdeb60c-d2ce-4ed9-af89-2b659c21f6e4
Creating secure boot keys...✓
Secure boot keys created!

❯ sbctl enroll-keys
Enrolling keys to EFI variables...✓
Enrolled keys to the EFI variables!

❯ sbctl status
Installed:      ✓ sbctl is installed
Owner GUID:     4cdeb60c-d2ce-4ed9-af89-2b659c21f6e4
Setup Mode:     ✓ Disabled
Secure Boot:    ✗ Disabled
Vendor Keys:    none
```
