!!! warning "Gentoo installation complete and stable"

    This chapter expects for the Gentoo installation to already be completed with use of the guide. Make sure that your Gentoo installation is working and stable. Don't update your Gentoo installation, for example, while following the instructions in this chapter. It also makes only sense to follow the instructions in this chapter in order to upgrade from an old SystemRescue version to a newer one.

!!! warning "Backup before upgrade"

    You may want to backup `/etc/gentoo-installation/systemrescuecd` as well as EFI System Partitions (ESPs) before trying a "Rescue System" upgrade.

## 13.1. Basic Virtual Machine Setup

If you didn't follow the steps outlined in chapter [13. Virtual Server (optional)](../optional_gentoo_guides/virtual_server.md), either follow those steps now or go with a basic virtual machine setup as follows:

```shell linenums="1"
echo "app-emulation/qemu -curl" >> /etc/portage/package.use/main && \
emerge --oneshot app-emulation/qemu acct-user/qemu acct-group/qemu && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

If you want to add Qemu to your world file, execute:

```shell linenums="1"
emerge --noreplace --select app-emulation/qemu acct-user/qemu acct-group/qemu
```

## 13.2. Downloads and Verification

Follow the steps outlined in subchapter [4.1. Downloads And Verification](rescue_system.md#41-downloads-and-verification) to download and verify a recent version of SystemRescue.

## 13.3. VM Preparation and Startup

Create a sufficiently sized .raw disk file at a location accessible to user `qemu`. This file will be deleted after completion of the upgrade. We don't worry about disk partitioning.

```shell linenums="1"
/bin/bash -c '
temp_mountpoint=$(mktemp -d) && \
truncate -s 5G /systemrescue.raw && \
mkfs.ext4 /systemrescue.raw && \
mount -o noatime /systemrescue.raw "${temp_mountpoint}" && \
mkdir -p "${temp_mountpoint}/gentoo/etc/gentoo-installation/systemrescuecd" && \
rsync -HSav --delete \
    --exclude="/work/*" \
    --exclude=/systemrescue_custom.iso \
    --exclude=/recipe/iso_add/sysresccd/zz_additional_packages.srm \
    /etc/gentoo-installation/systemrescuecd/ \
    "${temp_mountpoint}/gentoo/etc/gentoo-installation/systemrescuecd/" && \
umount "${temp_mountpoint}" && \
chown qemu:qemu /systemrescue.raw && \
echo -e "\e[1;32mSUCCESS\e[0m"
'
```

Startup a virtual machine using the newly downloaded SystemRescue .iso file:

```shell linenums="1"
su -l qemu -s /bin/bash -c "
    qemu-system-x86_64 \
        -m 2048 \
        -enable-kvm \
        -cdrom /etc/gentoo-installation/systemrescuecd/systemrescue.iso \
        -hda /systemrescue.raw \
        -serial mon:stdio \
        -nographic \
        --sandbox on,obsolete=deny,elevateprivileges=deny,spawn=deny,resourcecontrol=deny
    "
```

<figure markdown="span">
    <video controls controlslist="nofullscreen nodownload" height="474" width="620">
        <source src="/videos/rescue_system_upgrade_bootup.mp4" type="video/mp4">
        <source src="/videos/rescue_system_upgrade_bootup.webm" type="video/webm">
    </video>
    <figcaption>Bootup with Qemu</figcaption>
</figure>

## 13.4. Work within Virtual Machine

!!! warning "Work within Virtual Machine"

    Execute the commands in this subchapter within the Virtual Machine!

Select the following boot menu item:

> Boot SystemRescue with serial console (ttyS0,115200n8)

After bootup, mount the disk:

```shell linenums="1"
mount -o noatime /dev/sda /mnt && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Integrate additional packages required for [chroot.sh](https://codeberg.org/duxsco/gentoo-installation/src/branch/main/bin/disk.sh#L285-L364) to work. The packages installed are the same as the ones installed at the end of subchapter [4.2. Configuration](rescue_system.md#42-configuration).

```shell linenums="1"
pacman -Sy clevis efitools libpwquality luksmeta sbctl sbsigntools systemd-ukify tpm2-tools && \
cowpacman2srm /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/iso_add/sysresccd/zz_additional_packages.srm && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Copy the `os-release` file:

```shell linenums="1"
rsync -av /usr/lib/os-release /mnt/gentoo/etc/gentoo-installation/systemrescuecd/
```

!!! info "File/folder structure"

    Now, you should have the same file/folder structure within the virtual machine as shown in [4.4. Folder Structure](rescue_system.md#44-folder-structure), but with an up-to-date `zz_additional_packages.srm`.

[Create a custom installation medium](https://www.system-rescue.org/scripts/sysrescue-customize/) with above changes:

```shell linenums="1"
sysrescue-customize \
    --auto --overwrite \
    -s /mnt/gentoo/etc/gentoo-installation/systemrescuecd/systemrescue.iso \
    -d /mnt/gentoo/etc/gentoo-installation/systemrescuecd/systemrescue_custom.iso \
    -r /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe \
    -w /mnt/gentoo/etc/gentoo-installation/systemrescuecd/work && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Shutdown the virtual machine:

```shell linenums="1"
umount /mnt && \
systemctl poweroff
```

## 13.5 Upgrade Installation

!!! warning "Work on bare-metal"

    As we've shutdown the virtual machine, we resume work on bare-metal - the virtual machine's host - obviously.

!!! warning "Export "luks_rescue_device" variable"

    In this subchapter, make sure to always have the `luks_rescue_device` environment variable set.

    Print out the shell you use:

    ```shell linenums="1"
    echo "$SHELL"
    ```

    === "If you use bash, ..."

        ... execute:

        ```shell linenums="1"
        if [[ -L /dev/disk/by-id/md-name-sysrescue:rescue31415md ]]; then
            export luks_rescue_device="/dev/disk/by-id/md-name-sysrescue:rescue31415md" && \
            echo -e "\e[1;32mSUCCESS\e[0m"
        elif [[ -L /dev/disk/by-partlabel/rescue31415part ]]; then
            export luks_rescue_device="/dev/disk/by-partlabel/rescue31415part" && \
            echo -e "\e[1;32mSUCCESS\e[0m"
        fi
        ```

    === "If you use fish, ..."

        ... execute:

        ```shell linenums="1"
        if [ -L /dev/disk/by-id/md-name-sysrescue:rescue31415md ]
            export luks_rescue_device="/dev/disk/by-id/md-name-sysrescue:rescue31415md" && \
            echo -e "\e[1;32mSUCCESS\e[0m"
        else if [ -L /dev/disk/by-partlabel/rescue31415part ]
            export luks_rescue_device="/dev/disk/by-partlabel/rescue31415part" && \
            echo -e "\e[1;32mSUCCESS\e[0m"
        end
        ```

Update the folder `/etc/gentoo-installation/systemrescuecd`:

```shell linenums="1"
/bin/bash -c '
temp_mountpoint=$(mktemp -d) && \
mount -o ro /systemrescue.raw "${temp_mountpoint}" && \
rsync -HSacv --delete "${temp_mountpoint}/gentoo/etc/gentoo-installation/systemrescuecd/" /etc/gentoo-installation/systemrescuecd/ && \
umount "${temp_mountpoint}" && \
echo -e "\e[1;32mSUCCESS\e[0m"
'
```

Open your System Rescue partition:

```shell linenums="1"
cryptsetup luksOpen "$luks_rescue_device" mapperRescue
```

Copy the content of the custom installation medium to the "rescue" partition:

```shell linenums="1"
/bin/bash -c '
temp_mountpoint_iso=$(mktemp -d) && \
temp_mountpoint_rescue=$(mktemp -d) && \
mount -o loop,ro /etc/gentoo-installation/systemrescuecd/systemrescue_custom.iso "${temp_mountpoint_iso}" && \
mount -o noatime /dev/mapper/mapperRescue "${temp_mountpoint_rescue}" && \
rsync -HSacv --delete "${temp_mountpoint_iso}"/{autorun,sysresccd,sysrescue.d} "${temp_mountpoint_rescue}/" && \
umount "${temp_mountpoint_iso}" "${temp_mountpoint_rescue}" && \
echo -e "\e[1;32mSUCCESS\e[0m"
'
```

Set `ukify` useflag for systemd:


```shell linenums="1"
echo "sys-apps/systemd ukify" >> /etc/portage/package.use/main && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Install `ukify`:

```shell linenums="1"
emerge -avuDN @world
```

Create the [unified kernel image](https://wiki.archlinux.org/title/Unified_kernel_image#ukify) which will be used to boot the rescue system:

```shell linenums="1"
/bin/bash -c '
temp_mountpoint_rescue=$(mktemp -d) && \
mount -o noatime /dev/mapper/mapperRescue "${temp_mountpoint_rescue}" && \
ukify build \
  --linux="${temp_mountpoint_rescue}/sysresccd/boot/x86_64/vmlinuz" \
  --initrd="${temp_mountpoint_rescue}/sysresccd/boot/x86_64/sysresccd.img" \
  --cmdline="cryptdevice=UUID=$(blkid -s UUID -o value ${luks_rescue_device}):root root=/dev/mapper/root archisobasedir=sysresccd archisolabel=rescue31415fs noautologin loadsrm=y" \
  --os-release=@/etc/gentoo-installation/systemrescuecd/os-release \
  --output=/tmp/systemrescuecd.efi && \
sbctl sign /tmp/systemrescuecd.efi && \
umount "${temp_mountpoint_rescue}" && \
cryptsetup luksClose mapperRescue && \
while read -r my_esp; do
  ( mountpoint -q "${my_esp}" || mount "${my_esp}" ) && \
  rsync -av "/tmp/systemrescuecd.efi" "${my_esp}/EFI/Linux/" && \
  echo -e "\e[1;32mSUCCESS\e[0m"
done < <(grep -Po "^UUID=[0-9A-F]{4}-[0-9A-F]{4}[[:space:]]+\K/boot/efi[a-z](?=[[:space:]]+vfat[[:space:]]+)" /etc/fstab)
'
```

Delete the .raw disk file:

```shell linenums="1"
rm /systemrescue.raw
```
