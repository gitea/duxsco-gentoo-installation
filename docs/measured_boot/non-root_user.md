!!! warning "Export "non_root" variable"

    In this chapter, make sure to always have the `non_root` environment variable set:

    ```shell linenums="1"
    # Replace with the name of your choice for your non-root user.
    export non_root="david"
    ```

    This variable will be used to setup your non-root user.

## 7.1. Account Creation

Create a [non-root user](https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation#Optional:_User_accounts):

```shell linenums="1"
useradd -m -s /bin/bash "${non_root}" && \
chmod u=rwx,og= "/home/${non_root}" && \
echo -e 'alias cp="cp -i"\nalias mv="mv -i"\nalias rm="rm -i"' >> "/home/${non_root}/.bash_aliases" && \
chown "${non_root}": "/home/${non_root}/.bash_aliases" && \
echo 'source "${HOME}/.bash_aliases"' >> "/home/${non_root}/.bashrc" && \
passwd "${non_root}"
```

## 7.2. Access Control

Setup [run0](https://www.freedesktop.org/software/systemd/man/256/run0.html) or [app-admin/sudo](https://wiki.gentoo.org/wiki/Sudo):

!!! example ""

    === "run0"

        Add support for "run0":

        ```shell linenums="1"
        echo "sys-apps/systemd policykit" >> /etc/portage/package.use/main && \
        echo -e "\e[1;32mSUCCESS\e[0m"
        ```

        Update and make sure "sys-apps/systemd" is listed among the packages:

        ```shell linenums="1"
        emerge -avuDN @world
        ```

        You just need to set a passphrase to be able to switch to the "root" user with `run0` or to execute a command like `whoami` as "root" with `run0 whoami`:

        ```shell linenums="1"
        passwd root
        ```

        If you want to see the systemd journal, add your non-root user to the "systemd-journal" group:

        ```shell linenums="1"
        gpasswd -a "${non_root}" systemd-journal
        ```

    === "sudo"

        In the following, the non-root user we previously created is added to the ["wheel" group](https://wiki.gentoo.org/wiki/FAQ#How_do_I_add_a_normal_user.3F) and thus has the privilege to use `sudo`:

        ```shell linenums="1"
        echo "app-admin/sudo -sendmail" >> /etc/portage/package.use/main && \
        emerge app-admin/sudo && \
        { [[ -d /etc/sudoers.d ]] || mkdir -m u=rwx,g=rx,o= /etc/sudoers.d; } && \
        echo "%wheel ALL=(ALL) ALL" | EDITOR="tee" visudo -f /etc/sudoers.d/wheel && \
        gpasswd -a "${non_root}" wheel && \
        echo -e "\e[1;32mSUCCESS\e[0m"
        ```

Setup SSH client config:

```shell linenums="1"
mkdir -m 0700 "/home/${non_root}/.ssh" && \
echo "AddKeysToAgent no
HashKnownHosts no
StrictHostKeyChecking ask
VisualHostKey yes" > "/home/${non_root}/.ssh/config" && \
chown -RP "${non_root}": "/home/${non_root}/.ssh" && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

## 7.3. ~/.bashrc and chroot

Add the following to "/root/.bashrc"
for [chroot.sh](https://codeberg.org/duxsco/gentoo-installation/src/branch/main/bin/disk.sh#L285-L364) to work:

```shell linenums="1"
echo '
# Use fish in place of bash
# keep this line at the bottom of ~/.bashrc
if [[ -z ${chrooted} ]]; then
    if [[ -x /bin/fish ]]; then
        SHELL=/bin/fish exec /bin/fish
    fi
elif [[ -z ${chrooted_su} ]]; then
    export chrooted_su=true
    source /etc/profile && su --login --whitelist-environment=chrooted,chrooted_su
else
    env-update && source /etc/profile && export PS1="(chroot) $PS1"
fi' >> /root/.bashrc
```
