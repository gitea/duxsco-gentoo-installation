!!! info "SystemRescue currently without Measured Boot"

    A [feature request](https://gitlab.com/systemrescue/systemrescue-sources/-/issues/292) has been opened to have SystemRescue support Measured Boot.

While we are still on SystemRescue and not within chroot, download the SystemRescue .iso file and create a customised one out of it.

## 4.1. Downloads And Verification

!!! warning "Export "non_root" variable"

    In this subchapter, make sure to always have the `non_root` environment variable set:

    ```shell linenums="1"
    export non_root=$(id -nu 1000)
    ```

    This variable will be used to run commands without root privileges.

!!! warning "Export "my_root" variable"

    In this subchapter, make sure to always have the `my_root` environment variable set.

    Print out the shell you use:

    ```shell linenums="1"
    echo "$SHELL"
    ```

    === "If you use bash, ..."

        ... execute:

        ```shell linenums="1"
        if ! mountpoint -q /mnt/gentoo; then
            export my_root="" && \
            echo -e "\e[1;32mSUCCESS\e[0m"
        elif [[ -L /mnt/gentoo/devRescue ]]; then
            mkdir -p /mnt/gentoo/etc/gentoo-installation/systemrescuecd && \
            export my_root="/mnt/gentoo" && \
            echo -e "\e[1;32mSUCCESS\e[0m"
        fi
        ```

    === "If you use fish, ..."

        ... execute:

        ```shell linenums="1"
        if ! mountpoint -q /mnt/gentoo
            export my_root="" && \
            echo -e "\e[1;32mSUCCESS\e[0m"
        else if [ -L /mnt/gentoo/devRescue ]
            mkdir -p /mnt/gentoo/etc/gentoo-installation/systemrescuecd && \
            export my_root="/mnt/gentoo" && \
            echo -e "\e[1;32mSUCCESS\e[0m"
        end
        ```

!!! info "SystemRescue Download Page"

    The files downloaded here are all linked on [SystemRescue's download page](https://www.system-rescue.org/Download/).

Change ownership of the working directory:

```shell linenums="1"
chown -RP "$non_root": "$my_root/etc/gentoo-installation/systemrescuecd" && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Import the GnuPG public key used to sign the SystemRescue .iso:

```shell linenums="1"
su -l "$non_root" -c "
mkdir --mode=0700 /tmp/gpg_home_dir_systemrescue && \
curl -fsSL --proto =https --tlsv1.3 https://www.system-rescue.org/security/signing-keys/gnupg-pubkey-fdupoux-20210704-v001.pem | gpg --homedir /tmp/gpg_home_dir_systemrescue --import && \
gpg --homedir /tmp/gpg_home_dir_systemrescue --import-ownertrust <<<'62989046EB5C7E985ECDF5DD3B0FEA9BE13CA3C9:6:' && \
gpgconf --homedir /tmp/gpg_home_dir_systemrescue --kill all && \
echo -e '\e[1;32mSUCCESS\e[0m'
"
```

Download the .iso and .asc files:

```shell linenums="1"
/bin/bash -c '
rescue_system_version=$(su -l "$non_root" -c "curl -fsS --proto =https --tlsv1.3 https://gitlab.com/systemrescue/systemrescue-sources/-/raw/main/VERSION") && \
su -l "$non_root" -c "
curl -L --proto =https --tlsv1.2 --ciphers ECDHE+AESGCM:ECDHE+CHACHA20 --output \"$my_root/etc/gentoo-installation/systemrescuecd/systemrescue.iso\" \"https://fastly-cdn.system-rescue.org/releases/${rescue_system_version}/systemrescue-${rescue_system_version}-amd64.iso\" && \
curl -fsSL --proto =https --tlsv1.3 --output \"$my_root/etc/gentoo-installation/systemrescuecd/systemrescue.iso.asc\" \"https://www.system-rescue.org/releases/${rescue_system_version}/systemrescue-${rescue_system_version}-amd64.iso.asc\" && \
echo -e \"\e[1;32mSUCCESS\e[0m\"
"
'
```

Verify the .iso file with GnuPG:

```shell linenums="1"
su -l "$non_root" --whitelist-environment=my_root -c '
    gpg_status=$(gpg --homedir /tmp/gpg_home_dir_systemrescue --batch --status-fd 1 --verify "$my_root/etc/gentoo-installation/systemrescuecd/systemrescue.iso.asc" "$my_root/etc/gentoo-installation/systemrescuecd/systemrescue.iso" 2>/dev/null) && \
    gpgconf --homedir /tmp/gpg_home_dir_systemrescue --kill all && \
    grep -E -q "^\[GNUPG:\][[:space:]]+GOODSIG[[:space:]]+" <<< "${gpg_status}" && \
    grep -E -q "^\[GNUPG:\][[:space:]]+VALIDSIG[[:space:]]+" <<< "${gpg_status}" && \
    grep -E -q "^\[GNUPG:\][[:space:]]+TRUST_ULTIMATE[[:space:]]+" <<< "${gpg_status}" && \
    echo -e "\e[1;32mSUCCESS (1/2)\e[0m"
' && \
chown -RP 0:0 "$my_root/etc/gentoo-installation/systemrescuecd" && \
echo -e "\e[1;32mSUCCESS (2/2)\e[0m"
```

!!! warning "Rescue System Upgrade"

    If you came to this subchapter from subchapter [13.2. Downloads and Verification](rescue_system_upgrade.md#132-downloads-and-verification), stop here and continue with subchapter [13.3. VM Preparation and Startup](rescue_system_upgrade.md#133-vm-preparation-and-startup).

## 4.2. Configuration

Create the folder structure which will contain SystemRescue customisations:

```shell linenums="1"
mkdir -p /mnt/gentoo/etc/gentoo-installation/systemrescuecd/{recipe/{iso_delete,iso_add/{autorun,sysresccd,sysrescue.d},iso_patch_and_script,build_into_srm/{etc/sysctl.d,usr/local/sbin}},work} && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Disable "magic SysRq" for [security sake](https://wiki.gentoo.org/wiki/Vlock#Disable_SysRq_key):

```shell linenums="1"
echo "kernel.sysrq = 0" > /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/build_into_srm/etc/sysctl.d/99sysrq.conf && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Copy [chroot.sh created by disk.sh](https://codeberg.org/duxsco/gentoo-installation/src/branch/main/bin/disk.sh#L285-L364):

```shell linenums="1"
rsync -av --numeric-ids --chown=0:0 --chmod=u=rwx,go=r /tmp/chroot.sh /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/build_into_srm/usr/local/sbin/ && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Copy the [firewall script](https://codeberg.org/duxsco/gentoo-installation/src/branch/main/bin/firewall.sh):

```shell linenums="1"
# set firewall rules upon bootup.
rsync -av --numeric-ids --chown=0:0 --chmod=u=rw,go=r /root/bin/firewall.sh /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/iso_add/autorun/autorun && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

(Optional) Use static IPs (copy&paste one command block after the other):

``` { .shell .no-copy linenums="1" }
# Sample IPv4 addresses from:
# https://datatracker.ietf.org/doc/html/rfc5737
IPV4_ADDRESS="192.168.2.2/24"
IPV4_GATEWAY="192.168.2.1"

# Sample IPv6 addresses from:
# https://datatracker.ietf.org/doc/html/rfc3849
IPV6_ADDRESS="2001:DB8::2/64"
IPV6_GATEWAY="2001:DB8::1"

# Sample DNS server from Freifunk Munich:
# https://ffmuc.net/wiki/doku.php?id=knb:dns
IPV4_DNS="5.1.66.255 185.150.99.255"
IPV6_DNS="2001:678:e68:f000:: 2001:678:ed0:f000::"

cat <<EOF >> /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/iso_add/autorun/autorun

nmcli conn mod "Wired connection 1" \\
ipv4.method    "manual" \\
ipv6.method    "manual" \\
ipv4.addresses "${IPV4_ADDRESS}" \\
ipv6.addresses "${IPV6_ADDRESS}" \\
ipv4.gateway   "${IPV4_GATEWAY}" \\
ipv6.gateway   "${IPV6_GATEWAY}" \\
ipv4.dns       "${IPV4_DNS}" \\
ipv6.dns       "${IPV6_DNS}" && \\
nmcli conn up "Wired connection 1"
EOF
```

Create the settings YAML (copy&paste one command block after the other):

``` { .shell .no-copy linenums="1" }
# set the password you want to use to login via TTY on the rescue system
crypt_pass=$(openssl passwd -6)

# set default settings
echo "\
---
global:
    copytoram: true
    checksum: true
    nofirewall: true
    loadsrm: true
    setkmap: de-latin1-nodeadkeys
    dostartx: false
    dovnc: false
    rootshell: /bin/bash
    rootcryptpass: '${crypt_pass}'

autorun:
    ar_disable: false
    ar_nowait: true
    ar_nodel: false
    ar_ignorefail: false\
" > /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/iso_add/sysrescue.d/500-settings.yaml

# unset the password variable
unset crypt_pass
```

Integrate additional packages required for [chroot.sh](https://codeberg.org/duxsco/gentoo-installation/src/branch/main/bin/disk.sh#L285-L364) to work:

```shell linenums="1"
pacman -Sy clevis efitools libpwquality luksmeta sbctl sbsigntools systemd-ukify tpm2-tools && \
cowpacman2srm /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/iso_add/sysresccd/zz_additional_packages.srm && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Copy the `os-release` file:

```shell linenums="1"
rsync -av /usr/lib/os-release /mnt/gentoo/etc/gentoo-installation/systemrescuecd/
```

## 4.3 (Optional) SSH Server

!!! info
    This section is only required if you want to access the rescue system over SSH.

Take care of [public key authentication](https://wiki.gentoo.org/wiki/SSH#Passwordless_authentication_to_a_distant_SSH_server) (copy&paste one command block after the other):

``` { .shell .no-copy linenums="1" }
mkdir -p /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/build_into_srm/root/.ssh

# add your ssh public keys to
# /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/build_into_srm/root/.ssh/authorized_keys

# set correct modes
chmod u=rwx,g=rx,o= /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/build_into_srm/root
chmod -R u=rwX,go= /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/build_into_srm/root/.ssh
```

Configure the SSH server:

```shell linenums="1"
mkdir -p /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/build_into_srm/etc/ssh && \

rsync -a /etc/ssh/sshd_config /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/build_into_srm/etc/ssh/sshd_config && \

# do some ssh server hardening
sed -i \
-e 's/^#Port 22$/Port 50023/' \
-e 's/^#X11Forwarding no$/X11Forwarding no/' /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/build_into_srm/etc/ssh/sshd_config && \

echo "
AuthenticationMethods publickey" >> /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/build_into_srm/etc/ssh/sshd_config && \

# create ssh_host_* files in build_into_srm/etc/ssh/
ssh-keygen -A -f /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/build_into_srm && \

{ diff /etc/ssh/sshd_config /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/build_into_srm/etc/ssh/sshd_config || true; } && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Open the SSH port:

```shell linenums="1"
echo "
iptables  -A INPUT -p tcp --dport 50023 -j ACCEPT -m conntrack --ctstate NEW
ip6tables -A INPUT -p tcp --dport 50023 -j ACCEPT -m conntrack --ctstate NEW" >> /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/iso_add/autorun/autorun
```

Write down fingerprints to double check upon initial SSH connection to the rescue system:

```shell linenums="1"
find /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe/build_into_srm/etc/ssh/ -type f -name "ssh_host*\.pub" -exec ssh-keygen -vlf {} \; && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

## 4.4. Folder Structure

After running through above installation steps, you should have the following file/folder structure:

!!! example ""

    === "SSH setup"
        ``` { .shell .no-copy linenums="1" }
        ❯ tree -a /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe
        /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe
        ├── build_into_srm
        │   ├── etc
        │   │   ├── ssh
        │   │   │   ├── sshd_config
        │   │   │   ├── ssh_host_ecdsa_key
        │   │   │   ├── ssh_host_ecdsa_key.pub
        │   │   │   ├── ssh_host_ed25519_key
        │   │   │   ├── ssh_host_ed25519_key.pub
        │   │   │   ├── ssh_host_rsa_key
        │   │   │   └── ssh_host_rsa_key.pub
        │   │   └── sysctl.d
        │   │       └── 99sysrq.conf
        │   ├── root
        │   │   └── .ssh
        │   │       └── authorized_keys
        │   └── usr
        │       └── local
        │           └── sbin
        │               └── chroot.sh
        ├── iso_add
        │   ├── autorun
        │   │   └── autorun
        │   ├── sysresccd
        │   │   └── zz_additional_packages.srm
        │   └── sysrescue.d
        │       └── 500-settings.yaml
        ├── iso_delete
        └── iso_patch_and_script
        
        16 directories, 13 files
        ```

    === "non-SSH setup"
        ``` { .shell .no-copy linenums="1" }
        ❯ tree -a /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe
        /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe
        ├── build_into_srm
        │   ├── etc
        │   │   └── sysctl.d
        │   │       └── 99sysrq.conf
        │   └── usr
        │       └── local
        │           └── sbin
        │               └── chroot.sh
        ├── iso_add
        │   ├── autorun
        │   │   └── autorun
        │   ├── sysresccd
        │   │   └── zz_additional_packages.srm
        │   └── sysrescue.d
        │       └── 500-settings.yaml
        ├── iso_delete
        └── iso_patch_and_script
        
        13 directories, 5 files
        ```

## 4.5. ISO And Rescue Partition

[Create a custom installation medium](https://www.system-rescue.org/scripts/sysrescue-customize/) with above changes:

```shell linenums="1"
sysrescue-customize \
    --auto --overwrite \
    -s /mnt/gentoo/etc/gentoo-installation/systemrescuecd/systemrescue.iso \
    -d /mnt/gentoo/etc/gentoo-installation/systemrescuecd/systemrescue_custom.iso \
    -r /mnt/gentoo/etc/gentoo-installation/systemrescuecd/recipe \
    -w /mnt/gentoo/etc/gentoo-installation/systemrescuecd/work && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Copy the content of the custom installation medium to the "rescue" partition:

```shell linenums="1"
mkdir /mnt/iso /mnt/gentoo/mnt/rescue && \
mount -o loop,ro /mnt/gentoo/etc/gentoo-installation/systemrescuecd/systemrescue_custom.iso /mnt/iso && \
mount -o noatime /mnt/gentoo/mapperRescue /mnt/gentoo/mnt/rescue && \
rsync -HSacv --delete /mnt/iso/{autorun,sysresccd,sysrescue.d} /mnt/gentoo/mnt/rescue/ && \
umount /mnt/iso && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

## 4.6. Kernel Installation

Create the [unified kernel image](https://wiki.archlinux.org/title/Unified_kernel_image#ukify) which will be used to boot the rescue system:

```shell linenums="1"
ukify build \
  --linux=/mnt/gentoo/mnt/rescue/sysresccd/boot/x86_64/vmlinuz \
  --initrd=/mnt/gentoo/mnt/rescue/sysresccd/boot/x86_64/sysresccd.img \
  --cmdline="cryptdevice=UUID=$(blkid -s UUID -o value /mnt/gentoo/devRescue):root root=/dev/mapper/root archisobasedir=sysresccd archisolabel=rescue31415fs noautologin loadsrm=y" \
  --os-release=@/usr/lib/os-release \
  --output=/tmp/systemrescuecd.efi && \
while read -r my_esp; do
  mkdir "${my_esp/devE/boot\/e}" && \
  mount -o noatime,dmask=0027,fmask=0137 "${my_esp}" "${my_esp/devE/boot\/e}" && \
  rsync -av "/tmp/systemrescuecd.efi" "${my_esp/devE/boot\/e}/" && \
  echo -e "\e[1;32mSUCCESS\e[0m"
done < <(find /mnt/gentoo/devEfi* -maxdepth 0)
```
