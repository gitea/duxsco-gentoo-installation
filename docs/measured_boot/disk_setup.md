!!! danger "Data Loss"

    You are expected to provide an empty disk for installation. Make sure to **backup important data** (and verify the backup) before going on with the guide! If your disk is not empty, you'll wipe it in the next subchapter!

## 3.1. Wiping Disks

[disk.sh](https://codeberg.org/duxsco/gentoo-installation/src/branch/main/bin/disk.sh) expects the disks, where you want to install Gentoo Linux on, to be completely empty. If that's not the case continue reading. Otherwise, continue with [3.2. Partitioning And Formating](#32-partitioning-and-formating).

If you use SSD(s) I recommend a [Secure Erase](https://wiki.archlinux.org/title/Solid_state_drive/Memory_cell_clearing). Alternatively, you can do a fast wipe the following way given that no LUKS, MDADM, SWAP etc. device is open on the disk (copy&paste one command block after the other):

``` { .shell .no-copy linenums="1" }
# Change disk name to the one you want to wipe
disk="/dev/sda"

for i in $(lsblk -npo kname "${disk}" | grep "^${disk}" | sort -r); do
    read -r -p "Do you want to wipe \"$i\"? Type uppercase \"yes\" to confirm. " wipe

    if [[ ${wipe} == YES ]]; then
        wipefs -a "$i"
    fi
done
```

!!! tip
    If you have confidential data stored in a non-encrypted way on HDD(s) and don't want to risk the data landing in foreign hands I recommend the use of something like `dd`, e.g. [https://wiki.archlinux.org/title/Securely_wipe_disk](https://wiki.archlinux.org/title/Securely_wipe_disk)!

## 3.2. Partitioning And Formating

!!! note
    You may want to execute the following codeblock outside `screen` and execute `clear` right after in order for confidential data not to show up when scrolling up.

!!! info "stage3 tarball download"

    In the following, a recent [stage3-amd64-hardened-systemd-<timestamp\>.tar.xz](https://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64-hardened-systemd/) file will be downloaded and verified by [disk.sh](https://codeberg.org/duxsco/gentoo-installation/src/branch/main/bin/disk.sh).

!!! info "swap space"

    You can specify the size of the swap space via `bash.sh -s <size in GiB>`. Visit the handbook to read on [swap space size recommendations](https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation#What_about_swap_space.3F).

Prepare the disks (copy&paste one command block after the other):

``` { .shell .no-copy linenums="1" }
# list devices
fdisk -l

# lookup all options
bash /root/bin/disk.sh -h

# disable bash history
set +o history

# adjust to your liking
bash /root/bin/disk.sh -f fallbackfallback -r rescuerescue -d "/dev/sda /dev/sdb etc." -s 12

# enable bash history
set -o history
```

!!! info
    [disk.sh creates the user "meh"](https://codeberg.org/duxsco/gentoo-installation/src/branch/main/bin/disk.sh#L258) which will be used later on to act as non-root.

## 3.3. /mnt/gentoo Content

After executing "disk.sh", the btrfs subvolume "@root" [mounted by "disk.sh" at "/mnt/gentoo/"](https://codeberg.org/duxsco/gentoo-installation/src/branch/main/bin/disk.sh#L256) should contain:

!!! example ""

    === "four disks"

        ``` { .shell .no-copy }
        ❯ tree -a /mnt/gentoo/
        /mnt/gentoo/
        ├── devEfia -> /dev/sda1
        ├── devEfib -> /dev/sdb1
        ├── devEfic -> /dev/sdc1
        ├── devEfid -> /dev/sdd1
        ├── devRescue -> /dev/md0
        ├── devSwapa -> /dev/sda3
        ├── devSwapb -> /dev/sdb3
        ├── devSwapc -> /dev/sdc3
        ├── devSwapd -> /dev/sdd3
        ├── devSystema -> /dev/sda4
        ├── devSystemb -> /dev/sdb4
        ├── devSystemc -> /dev/sdc4
        ├── devSystemd -> /dev/sdd4
        ├── mapperRescue -> /dev/mapper/md0
        ├── mapperSwap -> /dev/md1
        ├── mapperSystem -> /dev/mapper/sda4
        ├── stage3.tar.xz
        └── stage3.tar.xz.asc
        
        1 directory, 18 files
        ```

    === "three disks"

        ``` { .shell .no-copy }
        ❯ tree -a /mnt/gentoo/
        /mnt/gentoo/
        ├── devEfia -> /dev/sda1
        ├── devEfib -> /dev/sdb1
        ├── devEfic -> /dev/sdc1
        ├── devRescue -> /dev/md0
        ├── devSwapa -> /dev/sda3
        ├── devSwapb -> /dev/sdb3
        ├── devSwapc -> /dev/sdc3
        ├── devSystema -> /dev/sda4
        ├── devSystemb -> /dev/sdb4
        ├── devSystemc -> /dev/sdc4
        ├── mapperRescue -> /dev/mapper/md0
        ├── mapperSwap -> /dev/md1
        ├── mapperSystem -> /dev/mapper/sda4
        ├── stage3.tar.xz
        └── stage3.tar.xz.asc
        
        1 directory, 15 files
        ```

    === "two disks"

        ``` { .shell .no-copy }
        ❯ tree -a /mnt/gentoo/
        /mnt/gentoo/
        ├── devEfia -> /dev/sda1
        ├── devEfib -> /dev/sdb1
        ├── devRescue -> /dev/md0
        ├── devSwapa -> /dev/sda3
        ├── devSwapb -> /dev/sdb3
        ├── devSystema -> /dev/sda4
        ├── devSystemb -> /dev/sdb4
        ├── mapperRescue -> /dev/mapper/md0
        ├── mapperSwap -> /dev/md1
        ├── mapperSystem -> /dev/mapper/sda4
        ├── stage3.tar.xz
        └── stage3.tar.xz.asc
        
        1 directory, 12 files
        ```

    === "single disk"

        ``` { .shell .no-copy }
        ❯ tree -a /mnt/gentoo/
        /mnt/gentoo/
        ├── devEfia -> /dev/sda1
        ├── devRescue -> /dev/sda2
        ├── devSwapa -> /dev/sda3
        ├── devSystema -> /dev/sda4
        ├── mapperRescue -> /dev/mapper/sda2
        ├── mapperSwap -> /dev/mapper/sda3
        ├── mapperSystem -> /dev/mapper/sda4
        ├── stage3.tar.xz
        └── stage3.tar.xz.asc
        
        1 directory, 9 files
        ```

## 3.4. Tarball Extraction

!!! info "Manual verification of stage3 tarball signature"

    After download of the stage3 tarball, my [script verifies](https://codeberg.org/duxsco/gentoo-installation/src/branch/main/bin/disk.sh) the files by default. You can also verify the OpenPGP signature on your own as shown in the following codeblock. Make sure to compare the long key IDs printed out by following `gpg --list-keys` command with those posted on the [release media signatures](https://www.gentoo.org/downloads/signatures/) page. Copy&paste one command block after the other:

    ``` { .shell .no-copy linenums="1" }
    [root@sysrescue ~]# su -l meh -c "gpg --homedir=/tmp/gpg_home_dir_gentoo --list-keys"
    /tmp/gpg_home_dir_gentoo/pubring.kbx
    ------------------------------------
    pub   dsa1024 2004-07-20 [SC] [expires: 2025-07-01]
          D99EAC7379A850BCE47DA5F29E6438C817072058
    uid           [ unknown] Gentoo Linux Release Engineering (Gentoo Linux Release Signing Key) <releng@gentoo.org>
    sub   elg2048 2004-07-20 [E] [expires: 2025-07-01]

    pub   rsa4096 2009-08-25 [SC] [expires: 2026-07-01]
          13EBBDBEDE7A12775DFDB1BABB572E0E2D182910
    uid           [ultimate] Gentoo Linux Release Engineering (Automated Weekly Release Key) <releng@gentoo.org>
    sub   rsa2048 2019-02-23 [S] [expires: 2026-07-01]

    [root@sysrescue ~]# su -l meh -c "gpg --homedir=/tmp/gpg_home_dir_gentoo --verify /mnt/gentoo/stage3.tar.xz.asc /mnt/gentoo/stage3.tar.xz"
    gpg: Signature made Mon 22 Jul 2024 04:41:07 AM UTC
    gpg:                using RSA key 534E4209AB49EEE1C19D96162C44695DB9F6043D
    gpg: Good signature from "Gentoo Linux Release Engineering (Automated Weekly Release Key) <releng@gentoo.org>" [ultimate]

    [root@sysrescue ~]# su -l meh -c "gpgconf --homedir=/tmp/gpg_home_dir_gentoo --kill all"
    ```

[Extract the stage3 tarball](https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation#Installing_a_stage_file) and copy custom files:

```shell linenums="1"
tar -C /mnt/gentoo/ -xpvf /mnt/gentoo/stage3.tar.xz --xattrs-include='*.*' --numeric-owner && \
rsync -a --numeric-ids --chown=0:0 --chmod=u=rwx,go=r /root/bin/firewall.nft /mnt/gentoo/usr/local/sbin/ && \
rsync -a /root/bin/portage_hook_kernel /mnt/gentoo/root/ && \
rsync -a /root/bin/portage_hook_systemd /mnt/gentoo/root/ && \
mkdir -p /mnt/gentoo/etc/gentoo-installation && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

## 3.5. Mounting

Mount filesystems for the later chroot to work:

```shell linenums="1"
mkdir /mnt/gentoo/var/db/repos/gentoo && \
touch /mnt/gentoo/var/db/repos/gentoo/.keep && \
mount -o noatime,subvol=@ebuilds /mnt/gentoo/mapperSystem /mnt/gentoo/var/db/repos/gentoo && \
chown 250:250 /mnt/gentoo/var/db/repos/gentoo && \

# https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation#Mounting_the_root_partition
mount -t tmpfs -o noatime,nodev,nosuid,mode=1777,uid=root,gid=root tmpfs /mnt/gentoo/tmp && \

# https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation#Mounting_the_necessary_filesystems
mount --types proc /proc /mnt/gentoo/proc && \
mount --rbind /sys /mnt/gentoo/sys && \
mount --make-rslave /mnt/gentoo/sys && \
mount --rbind /dev /mnt/gentoo/dev && \
mount --make-rslave /mnt/gentoo/dev && \
mount --bind /run /mnt/gentoo/run && \
mount --make-slave /mnt/gentoo/run && \

# I put /home, /var/cache/binpkgs, /var/cache/distfiles and /var/tmp
# on separate btrfs subvolumes to keep backups separate.

mount -o noatime,subvol=@home /mnt/gentoo/mapperSystem /mnt/gentoo/home && \

# https://wiki.gentoo.org/wiki//var/cache/binpkgs
# https://wiki.gentoo.org/wiki/Binary_package_guide
touch /mnt/gentoo/var/cache/binpkgs/.keep && \
mount -o noatime,subvol=@binpkgs /mnt/gentoo/mapperSystem /mnt/gentoo/var/cache/binpkgs && \

# https://wiki.gentoo.org/wiki//var/cache/distfiles
# https://wiki.gentoo.org/wiki/DISTDIR
touch /mnt/gentoo/var/cache/distfiles/.keep && \
mount -o noatime,subvol=@distfiles /mnt/gentoo/mapperSystem /mnt/gentoo/var/cache/distfiles && \

# https://wiki.gentoo.org/wiki/Portage_TMPDIR_on_tmpfs
touch /mnt/gentoo/var/tmp/.keep && \
mount -o noatime,subvol=@var_tmp /mnt/gentoo/mapperSystem /mnt/gentoo/var/tmp && \
chmod 1777 /mnt/gentoo/var/tmp && \

echo -e "\e[1;32mSUCCESS\e[0m"
```
