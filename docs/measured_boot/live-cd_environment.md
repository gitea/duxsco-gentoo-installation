!!! info "SystemRescue to official Gentoo Linux ISOs"

    A switch from SystemRescue to the official Gentoo Linux ISO is in planning (see [issue #4](https://codeberg.org/duxsco/gentoo-installation/issues/4)).

In the following, I am using [SystemRescue](https://www.system-rescue.org/), **not** the [official Gentoo Linux installation media](https://www.gentoo.org/downloads/), in order to make use of its capability to create custom installation media and setup the "rescue" partition with it's [chroot.sh script](https://codeberg.org/duxsco/gentoo-installation/src/branch/main/bin/disk.sh#L285-L364). If not otherwise stated, commands are executed as "root" on the remote machine where Gentoo Linux needs to be installed, in the beginning via TTY, later on over SSH. Most of the time, you can copy&paste the whole codeblock, but understand the commands first and make adjustments (e.g. IP address, disk names) if required.

## 2.1 Special Environments

### 2.1.1. Virtual Machine

If you want to use a virtual machine for the upcoming setup either to test things out or just for your virtual machine to be a bit more secure, you have to take some preliminary measures. The easiest way to get started is the use of the [Virtual Machine Manager](https://wiki.gentoo.org/wiki/Virt-manager) with [QEMU](https://wiki.gentoo.org/wiki/QEMU):

!!! example ""

    === "1."
        Open the Virtual Machine Manager:

        ![screenshot of "Virtual Machine Manager"](../images/virtual_machine_manager_00.png)

    === "2."
        Create a new virtual machine:

        ![screenshot of "Virtual Machine Manager"](../images/virtual_machine_manager_01.png)

    === "3."
        Install from a local medium:

        ![screenshot of "Virtual Machine Manager"](../images/virtual_machine_manager_02.png)

    === "4."
        Select the SystemRescue ISO and OS variant "Gentoo Linux":

        ![screenshot of "Virtual Machine Manager"](../images/virtual_machine_manager_03.png)

    === "5."
        Select an appropiate amount of CPU cores and memory:

        ![screenshot of "Virtual Machine Manager"](../images/virtual_machine_manager_04.png)

    === "6."
        Enable storage and select a sufficiently sized disk:

        ![screenshot of "Virtual Machine Manager"](../images/virtual_machine_manager_05.png)

    === "7."
        Name your virtual machine, make sure to set the checkmark at "Customize configuration before install" and setup your network device:

        ![screenshot of "Virtual Machine Manager"](../images/virtual_machine_manager_06.png)

    === "8."
        Select an UEFI x86_64 firmware with support for Secure Boot and click on "Add Hardware" for the next step:

        ![screenshot of "Virtual Machine Manager"](../images/virtual_machine_manager_07.png)

    === "9."
        Select "TPM", configure as shown and click on "Finish":

        ![screenshot of "Virtual Machine Manager"](../images/virtual_machine_manager_08.png)

    === "10."
        A new "TPM v2.0" device should be listed. Click on "Begin Installation" for the next step:

        ![screenshot of "Virtual Machine Manager"](../images/virtual_machine_manager_09.png)

    === "11."
        Select an appropiate "Boot SystemRescue [...]" boot option and press ++enter++ :

        ![screenshot of "Virtual Machine Manager"](../images/virtual_machine_manager_10.png)

    === "12."
        The virtual machine should have booted into SystemRescue:

        ![screenshot of "Virtual Machine Manager"](../images/virtual_machine_manager_11.png)

### 2.1.2. Hetzner Dedicated Server

So far, I tested the guide successfully on a [Hetzner dedicated server with "AMD Ryzen 9 5950X" CPU](https://www.hetzner.com/sb/#search=5950x):

!!! example ""

    === "1."
        Upon bootup, press ++f8++ and enter setup:

        ![screenshot of "KVM console"](../images/hetzner_00.png)

    === "2."
        Load Optimized Defaults:

        ![screenshot of "KVM console"](../images/hetzner_01.png)

    === "3."
        Enable `AMD fTPM switch`:

        ![screenshot of "KVM console"](../images/hetzner_02.png)

    === "4."
        I did a `dd` of the `System Rescue` ISO to the internal disks. Thus, I prioritise `UEFI HDD` boot:

        ![screenshot of "KVM console"](../images/hetzner_03.png)

    === "5."
        Save & Reset:

        ![screenshot of "KVM console"](../images/hetzner_04.png)

    === "6."
        Press ++f8++ and enter setup again:

        ![screenshot of "KVM console"](../images/hetzner_05.png)

    === "7."
        Enable `Trusted Computing`:

        ![screenshot of "KVM console"](../images/hetzner_06.png)

    === "8."
        Clear Secure Boot Keys:

        ![screenshot of "KVM console"](../images/hetzner_07.png)

    === "9."
        Choose OS Type `Windows UEFI mode` and make sure to be in Secure Boot `Setup Mode`:

        ![screenshot of "KVM console"](../images/hetzner_08.png)

    === "10."
        Save & Reset:

        ![screenshot of "KVM console"](../images/hetzner_09.png)

    === "11."
        After having booted, you should be in UEFI mode, have a TPM 2.0 device and be in Secure Boot `Setup Mode`:

        ![screenshot of "KVM console"](../images/hetzner_10.png)

## 2.2. Live-CD Setup

Boot into SystemRescue and [set the correct keyboard layout](https://man7.org/linux/man-pages/man1/loadkeys.1.html):

```shell linenums="1"
loadkeys de-latin1-nodeadkeys
```

(Optional) Start Xfce (copy&paste one command block after the other):

``` { .shell .no-copy linenums="1" }
startx

# Open up a terminal in Xfce and set the keyboard layout:
setxkbmap de
```

Disable "magic SysRq" for [security sake](https://wiki.gentoo.org/wiki/Vlock#Disable_SysRq_key):

```shell linenums="1"
sysctl -w kernel.sysrq=0
```

!!! info "Using screen"
    You can detach from screen's session with ++ctrl+a+d++ and reattach with `screen -d -r install`. Scrolling works with ++ctrl+a+esc++ followed by ++up++ / ++down++ / ++page-up++ / ++page-down++ . You can exit "scroll mode" with ++esc++ .

Start a [screen](https://wiki.gentoo.org/wiki/Screen) session to better cope with networks disconnects. Alternatively, you can use [tmux](https://wiki.gentoo.org/wiki/Tmux).

```shell linenums="1"
screen -S install
```

If no automatic network setup has been done via DHCP, you have to use [nmtui](https://www.tecmint.com/nmtui-configure-network-connection/) (recommended over [nmcli](https://linux.die.net/man/1/nmcli)). On Xfce, you have the option to use [nm-applet](https://wiki.gentoo.org/wiki/NetworkManager#GTK_GUIs) in addition.

```shell linenums="1"
nmtui
```

Insert an iptables rule at the correct place for SystemRescue to accept SSH connection requests:

```shell linenums="1"
iptables  -I INPUT 4 -p tcp --dport 22 -j ACCEPT -m conntrack --ctstate NEW
ip6tables -I INPUT 4 -p tcp --dport 22 -j ACCEPT -m conntrack --ctstate NEW
```

Set a root password:

```shell linenums="1"
passwd root
```

Print out fingerprints to be able to double check later on upon initial SSH connection to the SystemRescue system:

```shell linenums="1"
find /etc/ssh/ -type f -name "ssh_host*\.pub" -exec ssh-keygen -vlf {} \;
```

Execute following "rsync" and "ssh" commands **on your local machine from within your local copy of the ["gentoo-installation" repo](https://codeberg.org/duxsco/gentoo-installation/)** (copy&paste one command block after the other):

``` { .shell .no-copy linenums="1" }
# Copy installation files to remote machine. Don't forget to set the correct IP.
rsync -e "ssh -o VisualHostKey=yes" -av --delete --chown=0:0 bin root@XXX:/root/

# From local machine, login into the remote machine
ssh root@...

# Resume "screen":
screen -d -r install
```

(Optional) Lock the screen on the remote machine by typing the following command on its keyboard (**not over SSH**) (copy&paste one command block after the other):

``` { .shell .no-copy linenums="1" }
# Execute "vlock" without any flags first.
vlock

# Try to relogin.

# If relogin doesn't work:
# 1. Switch TTY: <ctrl>+<alt>+<F2>
# 2. Set a correct password: passwd root
# 3. Switch to previous TTY: <ctrl>+<alt>+<F1>
# 4. Try to relogin again.

# If relogin succeeds execute vlock with flag "-a" to lock all TTY.
vlock -a
```

On bare-metal, [set the date and time](https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation#Setting_the_date_and_time) if the current system time is not correct:

```shell linenums="1"
! grep -q -w "hypervisor" <(grep "^flags[[:space:]]*:[[:space:]]*" /proc/cpuinfo) && \
# replace "MMDDhhmmYYYY" with UTC time
date --utc MMDDhhmmYYYY
```

On bare-metal, update the [hardware clock](https://wiki.gentoo.org/wiki/System_time#Hardware_clock):

```shell linenums="1"
! grep -q -w "hypervisor" <(grep "^flags[[:space:]]*:[[:space:]]*" /proc/cpuinfo) && \
hwclock --systohc --utc
```
