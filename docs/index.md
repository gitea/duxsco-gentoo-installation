Git commits and tags as well as release files auto-created by Codeberg are OpenPGP signed. [Release files are checked](https://codeberg.org/duxsco/gentoo-installation/src/branch/main/assets/check_sign_release.sh) prior to signing.

You can fetch my public key the following way:

```shell
gpg --locate-external-keys "d at myCodebergUsername dot de"
```

If above command doesn't work due to disabled [WKD](https://wiki.gnupg.org/WKD) in `~/.gnupg/gpg.conf` (or equivalent) you can do:

```shell
gpg --auto-key-locate clear,wkd --locate-external-keys "d at myCodebergUsername dot de"
```

If above command doesn't work either, GnuPG's own DNS resolver may get blocked by, for example, the corporate network you are connected to. You can fix this by using your operating system's DNS resolver. Just set the following option in `~/.gnupg/dirmngr.conf` or equivalent ([link](https://www.gnupg.org/documentation/manuals/gnupg/Dirmngr-Options.html#index-standard_002dresolver)) and execute `gpgconf --kill dirmngr`:

```
standard-resolver
```

Above [fix has also been introducted in gemato](https://github.com/projg2/gemato/pull/31) which is [used by the Gentoo Linux project](https://wiki.gentoo.org/wiki/Gemato).

If above `gpg --locate-external-keys` commands still don't work due to reasons:

```shell
duxsco_email="d at myCodebergUsername dot de"
wkd_url="$(gpg-wks-client --print-wkd-url "${duxsco_email}")"

curl --tlsv1.3 -o duxsco.gpg "${wkd_url}"
# or
wget --secure-protocol=TLSv1_3 --max-redirect=0 -O duxsco.gpg "${wkd_url}"

# Check whether everything is kosher before importing for real:
gpg --import-options show-only --import duxsco.gpg

# If everything is fine, import the public key:
gpg --key-origin wkd --import duxsco.gpg
```

## Public key verification

Unfortunately, OpenPGP web of trust in the large scale is dead. Fortunately, the Gentoo Linux project and me use WKD allowing for the preservation of third party signatures. So far, my public key is signed the following way:

```
My public key
├── gpg --keyserver hkps://keys.gentoo.org --recv-keys 0x84AD142F
└── gpg --locate-external-keys marecki at myFavouriteDistro dot org
    └── gpg --locate-external-keys openpgp-auth+l2-dev at myFavouriteDistro dot org
        └── gpg --locate-external-keys openpgp-auth+l1 at myFavouriteDistro dot org
```

btw, my favourite distro is Gentoo 😉

To assess my public key's signatures read on [PGP Web of Trust: Core Concepts Behind Trusted Communication](https://www.linux.com/training-tutorials/pgp-web-trust-core-concepts-behind-trusted-communication/), please.

You can find some info on Gentoo's OpenPGP CA here:

- [Project:Infrastructure/Authority Keys](https://wiki.gentoo.org/wiki/Project:Infrastructure/Authority_Keys)
- [GLEP 79: Gentoo OpenPGP Authority Keys](https://www.gentoo.org/glep/glep-0079.html)
- [Release media signatures](https://www.gentoo.org/downloads/signatures/)

## Revoked public keys

My public keys have a certain validity. After they expire, I revoke them if they are not needed anymore. These public keys are not provided over WKD in order to keep the public key there as slim as possible. If you need them for reasons, you can fetch over [HKPS](https://codeberg.org/duxsco/gpg-keyserver/):

```shell
# Fetch the revoked public keys:
gpg --auto-key-locate clear,hkps://revoked.duxsco.de --locate-external-keys "d at myCodebergUsername dot de"

# List the public keys:
gpg --list-options show-unusable-subkeys --list-keys "d at myCodebergUsername dot de"
```

## Why only WKD?

I used to provide my public key over DANE, [HKPS](https://codeberg.org/duxsco/gpg-keyserver) and WKD. DANE became irrelevant to me due to [bug T4618](https://dev.gnupg.org/T4618). HKPS has its own [drawbacks for providing 3rd party signatures](https://bugs.gentoo.org/878479). CERT, LDAP and NTDS share some of these drawbacks and/or are out of the question for public provisioning of public keys.
