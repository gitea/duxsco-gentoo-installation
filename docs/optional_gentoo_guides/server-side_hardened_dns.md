[Unbound](https://en.wikipedia.org/wiki/Unbound_(DNS_server)) is a validating, recursive, caching DNS resolver I recommend. [systemd-resolved](https://wiki.archlinux.org/title/systemd-resolved) is out of the question, because systemd folks don't point out experimental features in docs and/or manpages ([statement made by Lennart Poettering](https://github.com/systemd/systemd/issues/25676#issuecomment-1634810897)).

Do some preliminary setup of `net-dns/unbound`:

```shell hl_lines="5" linenums="1"
/bin/bash -c '
echo "net-dns/unbound ~amd64" >> /etc/portage/package.accept_keywords/main && \
emerge net-dns/unbound && \
( su -s /bin/sh -c "/usr/bin/unbound-anchor -a /etc/unbound/var/root-anchors.txt -c /etc/dnssec/icannbundle.pem" unbound || true ) && \
rsync -a /etc/unbound/unbound.conf /etc/unbound/._cfg0000_unbound.conf && \
sed -i \
-e "s|^#\(include: \"\)otherfile.conf\(\"\)$|\1/etc/unbound-resolv-conf.conf\2|" \
-e "s|^\([[:space:]]*\)# \(chroot: \"/etc/unbound\"\)$|\1\2|" \
-e "s|^\([[:space:]]*\)# \(hide-identity: \)no$|\1\2yes|" \
-e "s|^\([[:space:]]*\)# \(hide-version: \)no$|\1\2yes|" \
-e "s|^\([[:space:]]*\)# \(harden-short-bufsize: yes\)$|\1\2|" \
-e "s|^\([[:space:]]*\)# \(harden-large-queries: \)no$|\1\2yes|" \
-e "s|^\([[:space:]]*\)# \(harden-glue: yes\)$|\1\2|" \
-e "s|^\([[:space:]]*\)# \(harden-unverified-glue: \)no$|\1\2yes|" \
-e "s|^\([[:space:]]*\)# \(harden-dnssec-stripped: yes\)$|\1\2|" \
-e "s|^\([[:space:]]*\)# \(harden-below-nxdomain: yes\)$|\1\2|" \
-e "s|^\([[:space:]]*\)# \(harden-referral-path: \)no$|\1\2yes|" \
-e "s|^\([[:space:]]*\)# \(harden-algo-downgrade: \)no$|\1\2yes|" \
-e "s|^\([[:space:]]*\)# \(harden-unknown-additional: \)no$|\1\2yes|" \
-e "s|^\([[:space:]]*\)# \(qname-minimisation: yes\)$|\1\2|" \
-e "s|^\([[:space:]]*\)# \(qname-minimisation-strict: no\)$|\1\2|" \
-e "s|^\([[:space:]]*\)# \(use-caps-for-id: \)no$|\1\2yes|" \
-e "s|^\([[:space:]]*\)# \(private-address: 10.0.0.0/8\)$|\1\2|" \
-e "s|^\([[:space:]]*\)# \(private-address: 172.16.0.0/12\)$|\1\2|" \
-e "s|^\([[:space:]]*\)# \(private-address: 192.168.0.0/16\)$|\1\2|" \
-e "s|^\([[:space:]]*\)# \(private-address: 169.254.0.0/16\)$|\1\2|" \
-e "s|^\([[:space:]]*\)# \(private-address: fd00::/8\)$|\1\2|" \
-e "s|^\([[:space:]]*\)# \(private-address: fe80::/10\)$|\1\2|" \
-e "s|^\([[:space:]]*\)# \(private-address: ::ffff:0:0/96\)$|\1\2|" \
-e "s|^\([[:space:]]*\)# \(do-not-query-localhost: \)yes$|\1\2no|" \
-e "s|^\([[:space:]]*\)# \(minimal-responses: yes\)$|\1\2|" \
-e "s|^\([[:space:]]*\)# \(trust-anchor-file:\)\( \"/etc/dnssec/root-anchors.txt\"\)$|\1\2 \"/etc/unbound/var/root-anchors.txt\"|" \
/etc/unbound/._cfg0000_unbound.conf && \
echo -e "\e[1;32mSUCCESS\e[0m"
'
```

Modify `unbound-anchor.service`:

```shell linenums="1"
mkdir /etc/systemd/system/unbound-anchor.service.d && \
echo "\
[Service]
ExecStart=
ExecStart=/usr/bin/unbound-anchor -a /etc/unbound/var/root-anchors.txt -c /etc/dnssec/icannbundle.pem
User=unbound
Group=unbound" > /etc/systemd/system/unbound-anchor.service.d/override.conf && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Disable `systemd-resolved` and enable Unbound services:

```shell linenums="1"
systemctl enable unbound.service && \
systemctl enable unbound-anchor.service && \
systemctl disable systemd-resolved.service && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

??? info "Sample Configuration"

    In the following, you see the relevant parts of the `unbound.conf` for comparison purposes:

    ``` { .shell .no-copy linenums="1" }
    ❯ bash -c 'grep -v -e "^[[:space:]]*#" -e "^[[:space:]]*$" /etc/unbound/unbound.conf'
    include: "/etc/unbound-resolv-conf.conf"
    server:
            chroot: "/etc/unbound"
            hide-identity: yes
            hide-version: yes
            harden-short-bufsize: yes
            harden-large-queries: yes
            harden-glue: yes
            harden-unverified-glue: yes
            harden-dnssec-stripped: yes
            harden-below-nxdomain: yes
            harden-referral-path: yes
            harden-algo-downgrade: yes
            harden-unknown-additional: yes
            qname-minimisation: yes
            qname-minimisation-strict: no
            use-caps-for-id: yes
            private-address: 10.0.0.0/8
            private-address: 172.16.0.0/12
            private-address: 192.168.0.0/16
            private-address: 169.254.0.0/16
            private-address: fd00::/8
            private-address: fe80::/10
            private-address: ::ffff:0:0/96
            do-not-query-localhost: no
            minimal-responses: yes
            trust-anchor-file: "/etc/unbound/var/root-anchors.txt"
    python:
    dynlib:
    remote-control:
    ```

