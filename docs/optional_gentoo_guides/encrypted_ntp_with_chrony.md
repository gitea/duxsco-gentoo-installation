!!! info "Virtual Machines"

    A NTP client is usually not needed in virtual machines.

By default, no NTP client is running:

``` { .shell .no-copy linenums="1" }
❯ timedatectl status
               Local time: Mi 2024-07-14 07:42:21 CEST
           Universal time: Mi 2024-07-14 05:42:21 UTC
                 RTC time: Mi 2024-07-14 05:42:21
                Time zone: Europe/Berlin (CEST, +0200)
System clock synchronized: no
              NTP service: inactive # 👈 reflects the state of all NTP clients, not only systemd-timesyncd's
          RTC in local TZ: no
```

I recommend [chrony](https://wiki.gentoo.org/wiki/Chrony) over systemd's [systemd-timesyncd](https://wiki.archlinux.org/title/systemd-timesyncd) due to its support for Network Time Security (NTS).

```shell hl_lines="2" linenums="1"
emerge net-misc/chrony && \
rsync -a /etc/chrony/chrony.conf /etc/chrony/._cfg0000_chrony.conf && \
sed -i 's/^server/#server/' /etc/chrony/._cfg0000_chrony.conf && \
echo '
# Use NTP servers with NTS support
server ptbtime1.ptb.de iburst nts
server ptbtime2.ptb.de iburst nts
server ptbtime3.ptb.de iburst nts
server ptbtime4.ptb.de iburst nts

# NTS cookie jar to minimise NTS-KE requests upon chronyd restart
ntsdumpdir /var/lib/chrony

rtconutc

# Pin Let'\''s Encrypt'\''s "ISRG Root X1" root certificate
ntstrustedcerts /etc/ssl/certs/4042bcee.0
nosystemcert' >> /etc/chrony/._cfg0000_chrony.conf && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

??? info "TLS Certificate Pinning"

    `ptbtime[1-4].ptb.de` use certificates which rely on [Let's Encrypt's "ISRG Root X1" root certificate](https://letsencrypt.org/certificates/). Thus, I only allow this single root certificate:

    ``` { .shell .no-copy linenums="1" }
    # Make sure to execute the following command as non-root!
    ❯ echo Q | openssl s_client -connect ptbtime1.ptb.de:4460 -servername ptbtime1.ptb.de | sed -n '/^Certificate chain/,/^---/p'
    Connecting to 192.53.103.108
    depth=2 C=US, O=Internet Security Research Group, CN=ISRG Root X1
    verify return:1
    depth=1 C=US, O=Let's Encrypt, CN=R10
    verify return:1
    depth=0 CN=ptbtime1.ptb.de
    verify return:1
    DONE
    Certificate chain
     0 s:CN=ptbtime1.ptb.de
       i:C=US, O=Let's Encrypt, CN=R10
       a:PKEY: rsaEncryption, 3072 (bit); sigalg: RSA-SHA256
       v:NotBefore: Sep 22 03:03:36 2024 GMT; NotAfter: Dec 21 03:03:35 2024 GMT
     1 s:C=US, O=Let's Encrypt, CN=R10
       i:C=US, O=Internet Security Research Group, CN=ISRG Root X1
       a:PKEY: rsaEncryption, 2048 (bit); sigalg: RSA-SHA256
       v:NotBefore: Mar 13 00:00:00 2024 GMT; NotAfter: Mar 12 23:59:59 2027 GMT
    ---

    ❯ openssl x509 -noout -hash -subject -issuer -in /etc/ssl/certs/4042bcee.0
    4042bcee
    subject=C = US, O = Internet Security Research Group, CN = ISRG Root X1
    issuer=C = US, O = Internet Security Research Group, CN = ISRG Root X1
    ```

Enable the chrony service:

```shell linenums="1"
systemctl enable chronyd.service && \
echo -e "\e[1;32mSUCCESS\e[0m"
```
