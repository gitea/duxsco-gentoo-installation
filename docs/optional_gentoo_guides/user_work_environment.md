## VIM Editor

Setup [app-editors/vim](https://wiki.gentoo.org/wiki/Vim):

```shell hl_lines="3" linenums="1"
non_root="$(id -nu 1000)" && \
emerge -av app-editors/vim app-vim/molokai && \
rsync -a /etc/portage/make.conf /etc/portage/._cfg0000_make.conf && \
sed -i 's/^USE="\([^"]*\)"$/USE="\1 vim-syntax"/' /etc/portage/._cfg0000_make.conf && \
echo "filetype plugin on
filetype indent on
set number
set paste
syntax on
colorscheme molokai

if &diff
  colorscheme murphy
endif" | tee -a /root/.vimrc >> "/home/${non_root}/.vimrc"  && \
chown "${non_root}": "/home/${non_root}/.vimrc" && \
eselect editor set vi && \
eselect vi set vim && \
env-update && source /etc/profile && export PS1="(chroot) $PS1" && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

## starship, fish shell and nerd fonts

(Optional) If you have sufficient time, install non-binary `dev-lang/rust`:

```shell linenums="1"
emerge -1av dev-lang/rust
```

Install [app-shells/starship](https://starship.rs/):

```shell linenums="1"
non_root="$(id -nu 1000)" && \
echo "app-shells/starship ~amd64" >> /etc/portage/package.accept_keywords/main && \
emerge app-shells/starship && \
{ [[ -d /home/${non_root}/.config ]] || mkdir --mode=0700 "/home/${non_root}/.config"; } && \
{ [[ -d /root/.config ]] || mkdir --mode=0700 /root/.config; } && \
touch "/home/${non_root}/.config/starship.toml" && \
chown -RP "${non_root}": "/home/${non_root}/.config" && \
starship preset nerd-font-symbols | tee /root/.config/starship.toml > "/home/${non_root}/.config/starship.toml" && \
sed -i -e '/^\[hostname\]$/a format = "\[$hostname\](bold red) "' -e '/^\[hostname\]$/a ssh_only = false' /root/.config/starship.toml "/home/${non_root}/.config/starship.toml" && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Install [app-shells/fish](https://wiki.gentoo.org/wiki/Fish):

```shell hl_lines="5" linenums="1"
non_root="$(id -nu 1000)" && \
echo "=dev-libs/libpcre2-$(qatom -F "%{PVR}" "$(portageq best_visible / dev-libs/libpcre2)") pcre32" >> /etc/portage/package.use/main && \
echo "app-shells/fish ~amd64" >> /etc/portage/package.accept_keywords/main && \
emerge app-shells/fish && \
rsync -a /etc/portage/make.conf /etc/portage/._cfg0000_make.conf && \
sed -i 's/^USE="\([^"]*\)"$/USE="\1 fish-completion"/' /etc/portage/._cfg0000_make.conf && \
echo '
# Use fish in place of bash
# keep this line at the bottom of ~/.bashrc
if [[ -x /bin/fish ]]; then
    SHELL=/bin/fish exec /bin/fish
fi' >> "/home/${non_root}/.bashrc" && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Setup [auto-completion for the fish shell](https://wiki.archlinux.org/title/fish#Command_completion) (copy&paste one command block after the other):

``` { .shell .no-copy linenums="1" }
# root
/bin/fish -c fish_update_completions

# non-root
su -l "$(id -nu 1000)" -c "/bin/fish -c fish_update_completions"
```

Enable aliases and starship (copy&paste one command block after the other):

``` { .shell .no-copy linenums="1" }
non_root="$(id -nu 1000)"
su -
exit
su - "${non_root}"
exit
sed -i 's/^end$/    source "$HOME\/.bash_aliases"\n    starship init fish | source\nend/' /root/.config/fish/config.fish
sed -i 's/^end$/    source "$HOME\/.bash_aliases"\n    starship init fish | source\nend/' "/home/${non_root}/.config/fish/config.fish"
```

Install [nerd fonts](https://www.nerdfonts.com/):

```shell linenums="1"
non_root="$(id -nu 1000)" && \
emerge media-libs/fontconfig && \
su -l "${non_root}" -c "curl --proto =https --tlsv1.3 -fsSL -o /tmp/FiraCode.tar.xz https://github.com/ryanoasis/nerd-fonts/releases/download/v3.3.0/FiraCode.tar.xz" && \
sha256sum -c <<<"7c64c44d7e530b25faf23904e135e9d1ff0339a92ee64e35e7da9116aa48af67  /tmp/FiraCode.tar.xz" && \
mkdir /tmp/FiraCode && \
tar -C /tmp/FiraCode/ -xf /tmp/FiraCode.tar.xz && \
mkdir /usr/share/fonts/nerd-firacode && \
rsync -a --chown=0:0 --chmod=a=r /tmp/FiraCode/*.ttf /usr/share/fonts/nerd-firacode/ && \
echo -e "\e[1;32mSUCCESS\e[0m"
```
