Create the `@guru` subvolume and change ownership to the `portage` user in order to have the sync done by `portage` instead of `root`:

``` { .shell hl_lines="7" linenums="1" }
export my_temp_mountpoint="$(mktemp -d)" && \
mount -o noatime /dev/disk/by-label/system31415fs "$my_temp_mountpoint" && \
btrfs subvolume create "$my_temp_mountpoint/@guru" && \
chown portage:portage "$my_temp_mountpoint/@guru" && \
chmod u=rwx,go=rx "$my_temp_mountpoint/@guru" && \
umount "$my_temp_mountpoint" && \
rsync -a /etc/fstab /etc/._cfg0000_fstab && \
sed -n '/@ebuilds/ s/gentoo/guru  /; s/@ebuilds/@guru   /p' /etc/fstab >> /etc/._cfg0000_fstab && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Mount the `@guru` subvolume:

```shell linenums="1"
systemctl daemon-reload && \
mkdir -p /var/db/repos/guru && \
mount /var/db/repos/guru && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Install `dev-vcs/git` required for the GURU overlay sync later on:

```shell linenums="1"
emerge -av dev-vcs/git
```

Mask GURU overlay packages by default in order to make selective package unmasking mandatory:

```shell linenums="1"
echo "*/*::guru" > /etc/portage/package.mask/guru && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

You have two options to enable the overlay, both with advantages and disadvantages:

|                                     | Gentoo AnonGit     | GitHub             |
| :---------------------------------- | :----------------: | :----------------: |
| Git-server hosted by Gentoo project | :white_check_mark: | :x:                |
| Metadata cache of ebuild repository | :x:                | :white_check_mark: |
| TLS certificate pinning             | :white_check_mark: | :x:                |

!!! example ""

    === "Gentoo AnonGit"

        !!! danger "Overlay Security"

            When it comes to repo syncing later on, beware that the metadata information for the Portage package database isn't provided for the GURU overlay over Gentoo AnonGit, and you have to generate it on your own the following way in case you need it and given you are willing to [trust the GURU overlay](https://wiki.gentoo.org/wiki/Project:GURU#Disclaimer):

            - `eix-sync`: Keep the default setting of `OVERLAY_CACHE_METHOD`
            - `emaint`: Use `egencache` or `pmaint regen`

        Enable the GURU overlay:

        ```shell linenums="1"
        mkdir -p /etc/portage/repos.conf && \
        echo "\
        [guru]
        location = /var/db/repos/guru
        sync-type = git
        sync-uri = https://anongit.gentoo.org/git/repo/proj/guru.git" > /etc/portage/repos.conf/guru.conf && \
        echo -e "\e[1;32mSUCCESS\e[0m"
        ```

        (Optional) Pin to [Let's Encrypt's `ISRG Root X1` root certificate](https://letsencrypt.org/certificates/#root-cas) and enforce HTTPS as well as TLSv1.3 ([more info](https://codeberg.org/duxsco/gentoo-git)):

        ```shell linenums="1"
        git config --system includeIf.gitdir:/var/db/repos/guru/.path /etc/portage/gitconfig_guru && \
        git config --file /etc/portage/gitconfig_guru http.sslCAInfo /etc/ssl/certs/4042bcee.0 && \
        git config --file /etc/portage/gitconfig_guru http.sslCAPath /etc/ssl/certs/4042bcee.0 && \
        git config --file /etc/portage/gitconfig_guru http.sslVersion tlsv1.3 && \
        git config --file /etc/portage/gitconfig_guru protocol.allow never && \
        git config --file /etc/portage/gitconfig_guru protocol.https.allow always && \
        echo -e "\e[1;32mSUCCESS\e[0m"
        ```

    === "GitHub"

        Follow the steps for end users (**excluding** repo syncing) outlined by the GURU project: [Project:GURU/Information for End Users](https://wiki.gentoo.org/wiki/Project:GURU/Information_for_End_Users)

!!! example ""

    Sync the repo with either of the following two options depending on what you have opted for in subchapter [6.2. Repo Syncing](../measured_boot/portage_setup.md#62-repo-syncing):

    === "eix-sync"

        The `OVERLAY_CACHE_METHOD` setting of `app-portage/eix` defaults to `parse|ebuild*` ([source](https://github.com/vaeth/eix/blob/8c13aa0e1c7938dfdc6b786957d6a4d4f2e9681d/manpage/en-eix.1.in#L2801)) which are explained [here](https://github.com/vaeth/eix/blob/8c13aa0e1c7938dfdc6b786957d6a4d4f2e9681d/manpage/en-eix.1.in#L2985-L3001). For [security reasons](https://github.com/vaeth/eix/blob/8c13aa0e1c7938dfdc6b786957d6a4d4f2e9681d/manpage/en-eix.1.in#L2806-L2808), the change of the default setting is recommended:

        ```shell linenums="1"
        echo 'OVERLAY_CACHE_METHOD="parse"' > /etc/eixrc/01-cache
        ```

        Do the repo sync itself:

        ```shell linenums="1"
        eix-sync
        ```

    === "emaint (replaced "emerge --sync")"

        ```shell linenums="1"
        emaint --auto sync
        ```

!!! warning "Installation Resumption"

    If you came to this chapter from subchapter [12.6.1. GURU Overlay](../measured_boot/post-boot_configuration.md#1261-guru-overlay), continue with subchapter [12.6.2. Installation](../measured_boot/post-boot_configuration.md#1262-installation).
