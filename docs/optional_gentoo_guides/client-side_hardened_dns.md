The following configuration is intended for laptops that may connect to foreign hotspots, such as those provided in hotels or at conferences, where the user has to agree to the terms of service in order to access the Internet, but later wishes to automatically switch to a DNS server of their choice that is:

- Reliable: DNS lookups not susceptible to blocking, filterting and other kinds of manipulation by third parties (e.g. Internet service or hotspot providers); if you want to filter out ads use [uBlock Origin](https://en.wikipedia.org/wiki/UBlock_Origin)
- Secure: No logging and eavesdropping of DNS traffic

!!! info "Self-hosting DNS Servers"

    In general, I recommend self-hosting a DNS server over a secure channel, e.g. DNSCrypt. However, to keep things simple, the following configuration uses:

    - [DNSCrypt servers](https://fr.dnscrypt.info) provided by [Frank Denis](https://github.com/jedisct1), one of the core maintainers of DNSCrypt
    - [DNSCrypt relays](https://www.dnscry.pt/) provided by Alexander Brügmann
    - [Bootstrap resolvers](https://ffmuc.net/wiki/doku.php?id=knb:dns) provided by [Freifunk](https://en.wikipedia.org/wiki/Freifunk)

    You can find alternative:

    - DNSCrypt servers and relays at [https://dnscrypt.info/public-servers](https://dnscrypt.info/public-servers)
    - Bootstrap resolvers on [this German website](https://www.kuketz-blog.de/empfehlungsecke/#dns)

    Change the servers and relays to suit your needs.

## Custom dhcpcd resolv.conf hook

As initially stated, I want to make use of foreign hotspots, but switch over to a reliable and secure DNS server once Internet access has been granted. This is possible by patching dhcpcd's `hooks/20-resolv.conf` and results in following behaviour of dhcpcd:

!!! example ""

    ``` mermaid
    graph LR
      A@{ shape: circle, label: "Start" };
      B{Is connectivitycheck.grapheneos.network reachable over HTTP?};
      C{Did 2 minutes pass?};
      D[Wait 5 seconds for the user to visit the captive portal, e.g. of the hotel hotspot, with their web browser and agree to the terms of service.];
      E{Is a default route set to a WireGuard network device?};
      F["Use anonymized DNSCrypt"];
      G[Keep DNS server provided by WireGuard VPN];
      A --> B
      B --> |No| C;
      B --> |Yes| E;
      C --> |No| D;
      C --> |Yes| E;
      D --> B;
      E --> |No| F;
      E --> |Yes| G;
    ```

## DNSCrypt

Install `net-dns/dnscrypt-proxy`:

```shell linenums="1"
emerge -av net-dns/dnscrypt-proxy && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Configure `net-dns/dnscrypt-proxy` (copy&paste one command block after the other):

``` { .shell hl_lines="4" .no-copy linenums="1" }
# Switch over to bash for simplicity sake
bash --norc

rsync -a /etc/dnscrypt-proxy/dnscrypt-proxy.toml /etc/dnscrypt-proxy/._cfg0000_dnscrypt-proxy.toml && \
sed -i \
-e "s|^# \(server_names = \[\).*\(\]\)$|\1'scaleway-ams', 'scaleway-ams-ipv6', 'scaleway-fr', 'scaleway-fr-ipv6'\2|" \
-e "s|^\(require_dnssec = \)false$|\1true|" \
-e "s|^# \(tls_disable_session_tickets = \)false$|\1true|" \
-e "s|^\(bootstrap_resolvers = \[\).*\(\]\)$|\1'5.1.66.255:53', '185.150.99.255:53', '[2001:678:e68:f000::]:53', '[2001:678:ed0:f000::]:53'\2|" \
-e "s|^\(\[anonymized_dns\]\)$|\1\n\nroutes = [\n   { server_name='scaleway-ams',      via=['dnscry.pt-anon-amsterdam-ipv4', 'dnscry.pt-anon-amsterdam02-ipv4'] },\n   { server_name='scaleway-ams-ipv6', via=['dnscry.pt-anon-amsterdam-ipv6', 'dnscry.pt-anon-amsterdam02-ipv6'] },\n   { server_name='scaleway-fr',       via=['dnscry.pt-anon-frankfurt-ipv4', 'dnscry.pt-anon-frankfurt02-ipv4'] },\n   { server_name='scaleway-fr-ipv6',  via=['dnscry.pt-anon-frankfurt-ipv6', 'dnscry.pt-anon-frankfurt02-ipv6'] }\n]|" \
-e "s|^\(skip_incompatible = \)false$|\1true|" \
/etc/dnscrypt-proxy/._cfg0000_dnscrypt-proxy.toml && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Enable the `dnscrypt-proxy` service:

```shell linenums="1"
systemctl enable dnscrypt-proxy.service && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

## openresolv

I prefer `net-dns/openresolv` to manage DNS information. It works well with `net-misc/dhcpcd`.

Apply the configuration for `net-dns/openresolv` to be used instead of `systemd-resolved`:

```shell linenums="1"
echo "sys-apps/systemd -resolvconf" >> /etc/portage/package.use/main && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Install `net-dns/openresolv`:

```shell linenums="1"
emerge --oneshot net-dns/openresolv sys-apps/systemd && \
emerge --noreplace --select net-dns/openresolv && \
systemctl disable systemd-resolved.service && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

## dhcpcd

!!! info "WireGuard and DNS"

    WireGuard is only considered by the patched `hooks/20-resolv.conf` if it sets the default network route through the WireGuard network device, e.g. "wg0" or "wg0-mullvad". The patched dhcpcd hook expects for WireGuard to also set a DNS server. If that's not the case with your setup, update the following patch to fit your needs.

Download and verify the `net-misc/dhcpcd` patch (copy&paste one command block after the other):

``` { .shell .no-copy linenums="1"}
# Switch to non-root user
su -l "$(id -nu 1000)"

# Switch to bash shell
bash --norc

# Set the correct e-mail address
gpg_mail="d at myCodebergUsername dot de"

(
    cd /tmp/ && \
    curl -fsS --proto =https --tlsv1.3 --remote-name-all \
        https://codeberg.org/duxsco/gentoo-installation/raw/branch/main/patches/dhcpcd/{20-resolv.conf.patch,sha512.txt{,.asc}} && \
    export GNUPGHOME=$(mktemp -d) && \
    gpg --locate-external-keys "${gpg_mail}" && \
    echo "3AAE5FC903BB199165D4C02711BE5F68440E0758:6:" | gpg --batch --import-ownertrust --quiet && \
    gpg_status=$(gpg --batch --status-fd 1 --verify sha512.txt.asc sha512.txt 2>/dev/null) && \
    gpgconf --kill all && \
    grep -E -q "^\[GNUPG:\][[:space:]]+GOODSIG[[:space:]]+" <<< "${gpg_status}" && \
    grep -E -q "^\[GNUPG:\][[:space:]]+VALIDSIG[[:space:]]+" <<< "${gpg_status}" && \
    grep -E -q "^\[GNUPG:\][[:space:]]+TRUST_ULTIMATE[[:space:]]+" <<< "${gpg_status}" && \
    sha512sum --quiet -c sha512.txt && \
    echo -e "\e[1;32mSUCCESS\e[0m"
)
```

Enable the patch:

```shell linenums="1"
mkdir -p /etc/portage/patches/net-misc/dhcpcd/ && \
rsync -av --chown 0:0 --chmod=a=r /tmp/20-resolv.conf.patch /etc/portage/patches/net-misc/dhcpcd/ && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

Install `net-misc/dhcpcd` (copy&paste one command block after the other):

``` { .shell hl_lines="3" .no-copy linenums="1" }
emerge -av net-misc/dhcpcd

rsync -av /etc/dhcpcd.conf /etc/._cfg0000_dhcpcd.conf

# If you use VPN, mitigate attacks such as TunnelVision
# https://mullvad.net/en/blog/evaluating-the-impact-of-tunnelvision
sed -i 's/^option classless_static_routes$/nooption classless_static_routes/' /etc/._cfg0000_dhcpcd.conf

# Recommended: Restrict the DHCP client to a certain interface
# If your Internet facing network interface isn't "wlan0",
# replace it with the correct interface name.
echo -e "allowinterfaces wlan0" >> /etc/._cfg0000_dhcpcd.conf


# Make sure to delete the relevant configuration at /etc/systemd/network/
systemctl enable dhcpcd.service
```

Disable `systemd-networkd.service`:

```shell linenums="1"
systemctl disable systemd-networkd.service && \
rm -fv /etc/systemd/network/50-dhcp.network && \
echo -e "\e[1;32mSUCCESS\e[0m"
```

## Testing DNS Resolution

After the reboot, you can test DNS resolution ([link](https://openwrt.org/docs/guide-user/services/dns/dot_unbound#testing)).
