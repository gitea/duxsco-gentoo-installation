# Gentoo Linux Installation

> ️⚠️ You can find information on my GnuPG public key in my [Codeberg profile README](https://codeberg.org/duxsco/)! ⚠️

The documentation can be found in the "docs/" folder. To run the site locally do:

1. Install as "root":

```shell
echo "dev-python/mkdocs ~amd64
dev-python/mkdocs-material-extensions ~amd64
dev-python/mkdocs-minify-plugin ~amd64
dev-python/mkdocs-material ~amd64" >> /etc/portage/package.accept_keywords/main

emerge -av dev-python/mkdocs dev-python/mkdocs-material dev-python/mkdocs-minify-plugin
```

2. Within the folder containing "mkdocs.yml", execute as "non-root":

```shell
mkdocs serve
```

## Other Gentoo Linux repos

https://codeberg.org/duxsco?sort=recentupdate&language=&q=gentoo-&tab=repositories
